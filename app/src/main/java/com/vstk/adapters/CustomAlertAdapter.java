package com.vstk.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vstk.R;

import java.util.ArrayList;

/**
 * @author Naresh
 */
public class CustomAlertAdapter extends BaseAdapter {
    Context context;

    ArrayList<String> gNames;

    public CustomAlertAdapter(Context context, ArrayList<String> tList) {
        this.context = context;
        this.gNames = tList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.custom_alert_list_item, parent, false);

            holder = new ViewHolder();
            holder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);
            holder.llBg = (LinearLayout) row.findViewById(R.id.llBg);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        holder.txtTitle.setText(gNames.get(position));

//        if (context instanceof RegisterAsBusiness) {
//            if (position == 0) {
//                holder.llBg.setBackgroundColor(context.getResources().getColor(R.color.header_red_color));
//            }
//        }

        return row;

    }

    static class ViewHolder {
        TextView txtTitle;
        ImageView imageItem;
        LinearLayout llBg;

    }

    @Override
    public int getCount() {
        if (gNames != null) {
            return gNames.size();
        }

        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (gNames != null) {
            return gNames.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}