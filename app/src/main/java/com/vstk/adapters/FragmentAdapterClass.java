package com.vstk.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.vstk.activities.MainActivity;

import java.util.ArrayList;

import com.vstk.model.ProductsModel;

/**
 * Created by JUNED on 5/30/2016.
 */
public class FragmentAdapterClass extends FragmentStatePagerAdapter {

    int TabCount;
    ArrayList<ProductsModel> arr = new ArrayList<>();

    ArrayList<ProductsModel> arr_Filter = new ArrayList<>();

    public FragmentAdapterClass(FragmentManager fragmentManager, int CountTabs, ArrayList<ProductsModel> arr) {

        super(fragmentManager);

        this.TabCount = CountTabs;
        this.arr = arr;
    }

    @Override
    public Fragment getItem(int position) {

        arr_Filter = new ArrayList<>();
        arr_Filter.clear();

        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i).getProductName().equalsIgnoreCase(MainActivity.arrTabName.get(position)))
                arr_Filter.add(arr.get(i));
        }
        return /*new Tab_1_Activity(arr_Filter, position)*/ null;
    }

    @Override
    public int getCount() {
        return TabCount;
    }
}