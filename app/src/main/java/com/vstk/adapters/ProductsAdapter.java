/*
 * Copyright (C) 2015 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vstk.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vstk.R;
import com.vstk.constants.AppConstants;
import com.vstk.model.ProductsModel;

import java.util.ArrayList;


/**
 * @author Paul Burke (ipaulpro)
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ItemViewHolder> {

    ArrayList<ProductsModel> arrActivitiesData = new ArrayList<>();
    private Activity context;
    public static boolean mIsLastItem = false;


    public ProductsAdapter(Activity context, ArrayList<ProductsModel> mItems) {
        this.context = context;
        this.arrActivitiesData = mItems;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.products_list_item, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }


    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        mIsLastItem = loadData(position);
        final ProductsModel model = arrActivitiesData.get(position);

//        holder.txtAmount.setText("₹" + model.getValue());
//        holder.txtDesc.setText(model.getDescription());
//
//        if (model.getValidity().contains("NA")) {
//            holder.txtValidity.setText("VALIDITY: " + model.getValidity());
//        } else
//            holder.txtValidity.setText("VALIDITY: " + model.getValidity() + " Days");
//
//        holder.txtTalkTime.setText("TALKTIME: ₹" + model.getTalktime());
//
//
//        holder.ll_main_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent data = new Intent();
//                data.putExtra(AppConstants.PRODUCTS_DATA, model);
//                context.setResult(context.RESULT_OK, data);
//                ((Activity) context).finish();
//
//            }
//        });

    }


    @Override
    public int getItemCount() {
        return arrActivitiesData.size();
    }

    /**
     * "handle" view that initiates a drag event when touched.
     */
    public static class ItemViewHolder extends RecyclerView.ViewHolder {

       // public final TextView txtAmount, txtDesc, txtValidity, txtTalkTime;
        //public final LinearLayout ll_main_layout;

        public ItemViewHolder(View itemView) {
            super(itemView);

//            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
//            txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
//            txtValidity = (TextView) itemView.findViewById(R.id.txtValidity);
//            txtTalkTime = (TextView) itemView.findViewById(R.id.txtTalkTime);
//            ll_main_layout = (LinearLayout) itemView.findViewById(R.id.ll_main_layout);
        }


    }

    public boolean loadData(int pos) {
        return pos == arrActivitiesData.size() - 1;
    }

}
