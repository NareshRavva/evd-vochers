package com.vstk.favourites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.vstk.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naresh R on 12-01-2017.
 */

public class SelectFavourite extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_fav);

        ButterKnife.bind(this);
        llBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llBack:
                finish();
                break;
        }
    }
}
