package com.vstk.model;

import java.io.Serializable;

/**
 * Created by Naresh R on 21-10-2016.
 */

public class ProductsModel implements Serializable{


    private String vendor_id,VendorLogoPath,vendorName;
    private String description, talktime, validity, value, offer, plan_name,denomCode, displyLogoPath;

    private String productID,productName,productLogoPath;

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductLogoPath() {
        return productLogoPath;
    }

    public void setProductLogoPath(String productLogoPath) {
        this.productLogoPath = productLogoPath;
    }

    public String getVendorLogoPath() {
        return VendorLogoPath;
    }

    public void setVendorLogoPath(String vendorLogoPath) {
        VendorLogoPath = vendorLogoPath;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getDisplyLogoPath() {
        return displyLogoPath;
    }

    public void setDisplyLogoPath(String displyLogoPath) {
        this.displyLogoPath = displyLogoPath;
    }

    public String getDenomCode() {
        return denomCode;
    }

    public void setDenomCode(String denomCode) {
        this.denomCode = denomCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTalktime() {
        return talktime;
    }

    public void setTalktime(String talktime) {
        this.talktime = talktime;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }


    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }
}
