package com.vstk.logger;

import android.content.Context;
import android.util.Log;

/**
 * @author Naresh R
 */

public class Logger {
    private static String TAG = "LOGGER";

    private static Context context = null;


    public static boolean init(Context context) {
        boolean initialized = true;

        // Operation 1: Its very important to have a valid context here
        if (context == null) {
            Log.e(TAG, "Error initializing the Mobizz Logger. Supplied context is null");
            return false;
        }
        Logger.context = context;

        return initialized;
    }


    public static void warn(String TAG, String message) {
        Log.w(TAG, message);
    }

    public static void info(String TAG, String message) {
        Log.i(TAG, message);
    }

    public static void fatal(String TAG, String message) {
        Log.e(TAG, message);
    }


    public static void logStackTrace(Exception e) {
        String exception = "";
        exception = e.toString() + "\n";
        StackTraceElement[] temp = e.getStackTrace();
        for (int i = 0; i < temp.length; i++) {
            exception = exception + "Mobizz Exception at " + temp[i].toString() + "\n";
        }

        Log.w(TAG, exception);

    }

    public static void logStackTrace(OutOfMemoryError e) {
        String exception = "";
        exception = e.toString() + "\n";
        StackTraceElement[] temp = e.getStackTrace();
        for (int i = 0; i < temp.length; i++) {
            exception = exception + "Mobizz Exception at " + temp[i].toString() + "\n";
        }

        Log.w(TAG, exception);
    }

}
