package com.vstk.services;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.vstk.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naresh R on 11-01-2017.
 */

public class ServicesActivity extends AppCompatActivity implements View.OnClickListener{

    private ArrayList<ServicesDataModel> arrData = new ArrayList<>();

    private RecyclerView.LayoutManager mLayoutManager;

    private ServicesListAdapter adapter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_list);

        ButterKnife.bind(this);

        llBack.setOnClickListener(this);

        addDummyData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llBack:
                finish();
                break;
        }

    }

    private void addDummyData() {

        ServicesDataModel model;

        model = new ServicesDataModel();
        model.setTitle("Pre-Paid Utility Vochers");
        model.setSubTitle("Electricity,Gas,Water");
        arrData.add(model);

//        model = new ServicesDataModel();
//        model.setTitle("Send Remittance");
//        model.setSubTitle("Mobile Top-up,Bill Payments");
//        arrData.add(model);

        model = new ServicesDataModel();
        model.setTitle("TV Subscription");
        model.setSubTitle("Services Providers for TV subscription");
        model.setIcon(R.drawable.dth_logo);
        arrData.add(model);

        setDataToAdapter();

    }

    /**
     *
     */
    private void setDataToAdapter() {
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new ServicesListAdapter(this, arrData);
        recyclerView.setAdapter(adapter);
    }
}
