package com.vstk.services;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.vstk.R;
import com.vstk.airtime.TopUpListAdapter;
import com.vstk.airtime.TopUpModel;
import com.vstk.components.textview.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naresh R on 11-01-2017.
 */

public class DthListActivity extends AppCompatActivity implements View.OnClickListener{

    private ArrayList<TopUpModel> arrData = new ArrayList<>();

    private RecyclerView.LayoutManager mLayoutManager;

    private TopUpListAdapter adapter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_list);

        ButterKnife.bind(this);

        txtTitle.setText("TV Subscription Operators");

        llBack.setOnClickListener(this);

        addDummyData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llBack:
                finish();
                break;
        }

    }

    private void addDummyData() {

        TopUpModel model;

        model = new TopUpModel();
        model.setTitle("Canal Plus");
        //model.setMediaIcon(R.drawable.logo_canal);
        arrData.add(model);

        model = new TopUpModel();
        model.setTitle("DSTV");
        //model.setMediaIcon(R.drawable.logo_dstv);
        arrData.add(model);

        model = new TopUpModel();
        model.setTitle("Bleu Sat");
        //model.setMediaIcon(R.drawable.logo_bleu);
        arrData.add(model);

        model = new TopUpModel();
        model.setTitle("Star Times");
        //model.setMediaIcon(R.drawable.logo_star_times);
        arrData.add(model);

        setDataToAdapter();

    }

    /**
     *
     */
    private void setDataToAdapter() {
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new TopUpListAdapter(DthListActivity.this, arrData);
        recyclerView.setAdapter(adapter);
    }
}
