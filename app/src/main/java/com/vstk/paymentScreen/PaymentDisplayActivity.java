package com.vstk.paymentScreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vstk.R;
import com.vstk.constants.AppConstants;
import com.vstk.favourites.SelectFavourite;
import com.vstk.scanTransfer.TransferActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naresh R on 11-01-2017.
 */

public class PaymentDisplayActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.txtScan)
    TextView txtScan;

    @BindView(R.id.txtFindFav)
    TextView txtFindFav;

    @BindView(R.id.txtProceed)
    TextView txtProceed;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_screen);

        ButterKnife.bind(this);
        llBack.setOnClickListener(this);
        txtScan.setOnClickListener(this);
        txtFindFav.setOnClickListener(this);
        txtProceed.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.llBack:
                finish();
                break;

            case R.id.txtScan:

                Intent transeFer = new Intent(this, TransferActivity.class);
                transeFer.putExtra(AppConstants.SHOW_SCAN, true);
                startActivity(transeFer);

                //finish();
                break;

            case R.id.txtFindFav:
                Intent fav = new Intent(this, SelectFavourite.class);
                startActivity(fav);
                break;
        }

    }
}
