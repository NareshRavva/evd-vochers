package com.vstk.accounts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.vstk.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naresh R on 10-01-2017.
 */

public class AccountsActivity extends AppCompatActivity implements View.OnClickListener {

//    @BindView(R.id.ratingBar1)
//    RatingBar ratingbar_default;

    @BindView(R.id.llBack)
    LinearLayout llBack;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);

        ButterKnife.bind(this);

        llBack.setOnClickListener(this);

        //ratingbar_default.setNumStars(5);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llBack:
                finish();
                break;
        }

    }
}
