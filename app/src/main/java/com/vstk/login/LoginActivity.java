package com.vstk.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.vstk.Api.QDInterface;
import com.vstk.Api.ServiceMethods;
import com.vstk.Api.ServiceRequest;
import com.vstk.Api.ServiceUrls;
import com.vstk.R;
import com.vstk.homeScreen.HomeScreen;
import com.vstk.logger.Logger;
import com.vstk.utils.AppUtils;
import com.vstk.utils.SharedPrefsUtils;

import org.json.JSONObject;

import java.math.BigInteger;
import java.security.SecureRandom;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

/**
 * Created by Naresh R on 10-01-2017.
 */


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.llLogin)
    LinearLayout llLogin;

    @BindView(R.id.etvName)
    EditText etvName;

    @BindView(R.id.etvPasswrd)
    EditText etvPasswrd;

    private String TAG = "LoginActivity";

    private String IMEI;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        llLogin.setOnClickListener(this);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        IMEI = telephonyManager.getDeviceId();

        if (SharedPrefsUtils.getAuthID(this) != null) {
            moveToHomeScreen();
            return;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.llLogin:

                if (etvName.getText().toString().length() < 1) {
                    AppUtils.showToast(LoginActivity.this, "Enter Username");
                    return;
                }
                if (etvPasswrd.getText().toString().length() < 1) {
                    AppUtils.showToast(LoginActivity.this, "Enter Password");
                    return;
                }

                login();

                break;
        }
    }


    private void login() {

        SharedPrefsUtils.setUserName(this, etvName.getText().toString());
        SharedPrefsUtils.setUserPaswrd(this, etvPasswrd.getText().toString());

        AppUtils.showDialog(LoginActivity.this);

        RestAdapter rAdapter = new RestAdapter.Builder().setEndpoint(ServiceUrls.BASE_URL).
                setLogLevel(RestAdapter.LogLevel.BASIC).build();
        QDInterface restInt = rAdapter.create(QDInterface.class);
        rAdapter.getLogLevel();

        TypedInput tInput = ServiceRequest.returnTypedArr(this, ServiceMethods.USER_LOGIN,
                new Object[]{
                        etvName.getText().toString(),
                        etvPasswrd.getText().toString(),
                        IMEI
                });

        restInt.userLogin("application/json", tInput, new Callback<Response>() {
            @Override
            public void success(Response result, Response response) {

                //parse json objt
                parseData(AppUtils.ConvertResponseToJSONobjt(result));

            }

            @Override
            public void failure(RetrofitError error) {

                AppUtils.dismissDialog();

                Logger.info(TAG, error.toString() + "--getCause--" + error.getCause() + "---getMessage--" + error.getMessage());
            }
        });
    }

    private void parseData(JSONObject jsonObject) {
        AppUtils.dismissDialog();

        //Logger.info(TAG, "response--->" + jsonObject);

        if (jsonObject != null && jsonObject.optInt("statuscode") == 0) {
            SharedPrefsUtils.setAuthID(this, jsonObject.optString("AuthCode"));
            SharedPrefsUtils.setTerminalID(this, jsonObject.optString("TerminalID"));
            SharedPrefsUtils.setRemainBalnc(this, jsonObject.optString("RetailerBalance"));
            SharedPrefsUtils.setRetailorName(this, jsonObject.optString("RetailerName"));

            moveToHomeScreen();

        } else {
            AppUtils.showAlert(LoginActivity.this, jsonObject.optString("responsemessage"));
        }

    }

    private void moveToHomeScreen() {
        Intent homeScreen = new Intent(this, HomeScreen.class);
        startActivity(homeScreen);
        finish();
    }
}
