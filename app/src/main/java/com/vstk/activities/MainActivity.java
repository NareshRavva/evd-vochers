package com.vstk.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.vstk.R;
import com.vstk.adapters.FragmentAdapterClass;
import com.vstk.constants.AppConstants;
import com.vstk.logger.Logger;
import com.vstk.model.ProductsModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    FragmentAdapterClass fragmentAdapter;

    private ArrayList<ProductsModel> arrPrdctsRecved = new ArrayList<>();

    private String TAG = "MainActivity";

    public static ArrayList<String> arrTabName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        arrPrdctsRecved = (ArrayList<ProductsModel>) getIntent().getSerializableExtra(AppConstants.PRODUCTS_DATA);

        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout1);
        viewPager = (ViewPager) findViewById(R.id.pager1);

        setSupportActionBar(toolbar);

        arrTabName = new ArrayList<>();

        if (arrPrdctsRecved != null) {
            for (int i = 0; i < arrPrdctsRecved.size(); i++) {
                if (!arrTabName.contains(arrPrdctsRecved.get(i).getProductName())) {
                    arrTabName.add(arrPrdctsRecved.get(i).getProductName());

                    Logger.info(TAG, "getPackage_name--->" + arrPrdctsRecved.get(i).getProductName());

                    tabLayout.addTab(tabLayout.newTab().setText("  " + arrPrdctsRecved.get(i).getProductName() + "  "));
                }
            }

            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

            fragmentAdapter = new FragmentAdapterClass(getSupportFragmentManager(), tabLayout.getTabCount(), arrPrdctsRecved);

            viewPager.setAdapter(fragmentAdapter);

            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab LayoutTab) {

                    viewPager.setCurrentItem(LayoutTab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab LayoutTab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab LayoutTab) {

                }
            });
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        Logger.info(TAG,"data--->"+data.getStringExtra(AppConstants.PRODUCTS_DATA));
//
//        super.onActivityResult(requestCode, resultCode, data);
//    }

}