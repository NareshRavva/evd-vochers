package com.vstk.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.vstk.R;
import com.vstk.adapters.CustomAlertAdapter;
import com.vstk.adapters.ProductsAdapter;
import com.vstk.components.textview.TextView;
import com.vstk.constants.AppConstants;
import com.vstk.logger.Logger;
import com.vstk.model.ProductsModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naresh R on 28-10-2016.
 */

public class BrowsePlans extends Activity {

    private ArrayList<ProductsModel> arr = new ArrayList<>();

    private RecyclerView recyclerView;

    private RecyclerView.LayoutManager mLayoutManager;

    private ProductsAdapter prdctsAdapter;

    private ArrayList<ProductsModel> arrPrdctsRecved = new ArrayList<>();

    private ArrayList<String> arrTabName;

    private String TAG = "BrowsePlans";

    @BindView(R.id.txtOperator)
    TextView txtOperator;

    @BindView(R.id.imgDown)
    ImageView imgDown;

    private String SELECTED_SERVICE_PROVIDER = null;

    ArrayList<ProductsModel> arr_Filter = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_plans);

        ButterKnife.bind(this);

        imgDown.setColorFilter(Color.BLACK);

        arrPrdctsRecved = (ArrayList<ProductsModel>) getIntent().getSerializableExtra(AppConstants.PRODUCTS_DATA);

        TitlesData();

        txtOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder dialog = new AlertDialog.Builder(BrowsePlans.this);

                // dialog.setTitle("List of likers");o
                dialog.setCancelable(true);

                View view = ((Activity) BrowsePlans.this).getLayoutInflater().inflate(R.layout.custom_alert_list,
                        null);

                // final String[] titles = {"Airtel", "Idea", "Vodafone", "ALL"};

                ListView list = (ListView) view.findViewById(R.id.list);
                Button btnCancel = (Button) view.findViewById(R.id.btnCancel);

                CustomAlertAdapter adapter = new CustomAlertAdapter(BrowsePlans.this, new ArrayList<String>(
                        arrTabName));

                list.setAdapter(adapter);

                dialog.setView(view);
                final AlertDialog OptionDialog = dialog.create();

                OptionDialog.show();

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SELECTED_SERVICE_PROVIDER = null;
                        OptionDialog.dismiss();
                    }
                });


                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        txtOperator.setText(arrTabName.get(position));
                        OptionDialog.dismiss();

                        filterData(arrTabName.get(position));


                    }
                });
            }
        });
    }

    private void filterData(String name) {

        arr_Filter = new ArrayList<>();
        arr_Filter.clear();

        for (int i = 0; i < arrPrdctsRecved.size(); i++) {
            if (arrPrdctsRecved.get(i).getProductName().equalsIgnoreCase(name))
                arr_Filter.add(arrPrdctsRecved.get(i));
        }

        setDataToAdapter(arr_Filter);
    }

    /**
     *
     */
    private void TitlesData() {
        arrTabName = new ArrayList<>();

        if (arrPrdctsRecved != null) {
            for (int i = 0; i < arrPrdctsRecved.size(); i++) {
                if (!arrTabName.contains(arrPrdctsRecved.get(i).getProductName())) {
                    arrTabName.add(arrPrdctsRecved.get(i).getProductName());

                    Logger.info(TAG, "getPackage_name--->" + arrPrdctsRecved.get(i).getProductName());
                }
            }
        }

        txtOperator.setText(arrTabName.get(0));
        filterData(arrTabName.get(0));
    }

    /**
     *
     * @param arr_Filter
     */
    private void setDataToAdapter(ArrayList<ProductsModel> arr_Filter) {

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager(BrowsePlans.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        prdctsAdapter = new ProductsAdapter(BrowsePlans.this, arr_Filter);
        recyclerView.setAdapter(prdctsAdapter);
    }
}
