package com.vstk.airtime;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;

import com.vstk.Api.QDInterface;
import com.vstk.Api.ServiceMethods;
import com.vstk.Api.ServiceRequest;
import com.vstk.Api.ServiceUrls;
import com.vstk.R;
import com.vstk.activities.BrowsePlans;
import com.vstk.adapters.CustomAlertAdapter;
import com.vstk.components.edittext.EditText;
import com.vstk.components.textview.TextView;
import com.vstk.constants.AppConstants;
import com.vstk.logger.Logger;
import com.vstk.model.ProductsModel;
import com.vstk.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

/**
 * Created by Naresh R on 21-10-2016.
 */


public class AirTime extends Activity {

    @BindView(R.id.llProceedPayment)
    LinearLayout llProceedPayment;

    @BindView(R.id.llPayMentRes)
    LinearLayout llPayMentRes;

    @BindView(R.id.rBtnOffer)
    RadioButton rBtnPrepaid;

    @BindView(R.id.rBtnRequest)
    RadioButton rBtnPostpaid;

    @BindView(R.id.txtOperator)
    TextView txtOperator;

    @BindView(R.id.txtBrowsePlans)
    TextView txtBrowsePlans;

    @BindView(R.id.etxtAmount)
    EditText etxtAmount;

    @BindView(R.id.etxtMobile)
    EditText etxtMobile;

    @BindView(R.id.txtDescription)
    TextView txtDescription;

    @BindView(R.id.txtExpirydate)
    TextView txtExpirydate;

    @BindView(R.id.txtPinnumber)
    TextView txtPinnumber;

    @BindView(R.id.txtAmount)
    TextView txtAmount;

    @BindView(R.id.llBack)
    LinearLayout llBack;


    private String DEAL_TYPE = "DEAL_TYPE";
    private String TYPE_POSTPAID = "POSTPAID";
    private String TYPE_PREPAID = "PREPAID";

    private String TAG = "AirTime";

    ProgressDialog progressDialog;

    private String SELECTED_SERVICE_PROVIDER = null;
    private String DENOM_CODE = null;

    private ArrayList<ProductsModel> arrTotalPrdcts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime);

        ButterKnife.bind(this);

        llPayMentRes.setVisibility(View.GONE);

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtBrowsePlans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SELECTED_SERVICE_PROVIDER == null) {
                    AppUtils.showToast(v, AirTime.this, "Please Select Operator");
                    return;
                }

                getProductsByService();
            }
        });

        rBtnPrepaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DEAL_TYPE = TYPE_PREPAID;
                rBtnPostpaid.setChecked(false);
                rBtnPrepaid.setChecked(true);
            }
        });

        rBtnPostpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DEAL_TYPE = TYPE_POSTPAID;
                rBtnPrepaid.setChecked(false);
                rBtnPostpaid.setChecked(true);
            }
        });

        txtOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder dialog = new AlertDialog.Builder(AirTime.this);

                // dialog.setTitle("List of likers");o
                dialog.setCancelable(true);

                View view = ((Activity) AirTime.this).getLayoutInflater().inflate(R.layout.custom_alert_list,
                        null);

                final String[] titles = {"Airtel", "Idea", "Vodafone", "ALL"};

                ListView list = (ListView) view.findViewById(R.id.list);
                Button btnCancel = (Button) view.findViewById(R.id.btnCancel);
//                btnCancel.setVisibility(View.GONE);


                CustomAlertAdapter adapter = new CustomAlertAdapter(AirTime.this, new ArrayList<String>(
                        Arrays.asList(titles)));

                list.setAdapter(adapter);

                dialog.setView(view);
                final AlertDialog OptionDialog = dialog.create();

                OptionDialog.show();

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SELECTED_SERVICE_PROVIDER = null;
                        OptionDialog.dismiss();
                    }
                });


                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (position == 0) {
                            OptionDialog.dismiss();
                            SELECTED_SERVICE_PROVIDER = "1";
                            txtOperator.setText(titles[0]);

                        }
                        if (position == 1) {
                            OptionDialog.dismiss();
                            SELECTED_SERVICE_PROVIDER = "2";
                            txtOperator.setText(titles[1]);
                        }

                        if (position == 2) {
                            OptionDialog.dismiss();
                            SELECTED_SERVICE_PROVIDER = "1";
                            txtOperator.setText(titles[2]);
                        }

                        if (position == 3) {
                            OptionDialog.dismiss();
                            SELECTED_SERVICE_PROVIDER = "ALL";
                            txtOperator.setText(titles[3]);
                        }

//                        if (position == 4) {
//                            SELECTED_SERVICE_PROVIDER = null;
//                            OptionDialog.dismiss();
//                        }
                    }
                });
            }
        });

        llProceedPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                proceedPayment(v);
            }
        });

    }

    private void proceedPayment(final View v) {

        {

            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading....");
            progressDialog.show();

            RestAdapter rAdapter = new RestAdapter.Builder().setEndpoint(ServiceUrls.BASE_URL).
                    setLogLevel(RestAdapter.LogLevel.BASIC).build();
            QDInterface restInt = rAdapter.create(QDInterface.class);
            rAdapter.getLogLevel();

            TypedInput tInput = ServiceRequest.returnTypedArr(this, ServiceMethods.GET_PRODUCTS_BY_VENDOR_ID,
                    new Object[]{});

            restInt.doPayment(DENOM_CODE, null,new Callback<Response>() {
                @Override
                public void success(Response result, final Response response) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();

                            if (response.getStatus() == 200) {
                                AppUtils.showToast(v, AirTime.this, "Your payment transaction successfull");

                            } else
                                AppUtils.showToast(v, AirTime.this, "payment transaction fails");

                            txtOperator.setText("");
                            etxtAmount.setText("");
                            etxtMobile.setText("");
                        }
                    }, 1000);


                    //parse json objt
                    parsePaymentData(AppUtils.ConvertResponseToJSONobjt(result));

                }

                @Override
                public void failure(RetrofitError error) {

                    progressDialog.dismiss();

                    Logger.info(TAG, error.toString() + "--getCause--" + error.getCause() + "---getMessage--" + error.getMessage());
                }
            });


        }
    }

    private void parsePaymentData(JSONObject objt) {

//        {"Comment":"","Description":"Prepaid Voucher","Expirydate":"11\/23\/2016 1:03:16 PM",
//                "Pinnumber":"37887981610","Respcode":0,"SequenceNumber":"82481","SerialNumber":"378879816","amount":""}

        if (objt != null) {

            llPayMentRes.setVisibility(View.VISIBLE);

            Logger.info(TAG, "Response--->" + objt.toString());
            txtDescription.setText("Description : " + objt.optString("Description"));
            txtExpirydate.setText("ExpiryDate : " + objt.optString("Expirydate"));
            txtPinnumber.setText("PinNumber : " + objt.optString("Pinnumber"));
            txtAmount.setText("Amount : " + objt.optString("amount"));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data == null)
            return;

        //Logger.info(TAG, "data-in home screen-->" + data.getStringExtra(AppConstants.PRODUCTS_DATA));

        ProductsModel model = (ProductsModel) data.getSerializableExtra(AppConstants.PRODUCTS_DATA);

        Logger.info(TAG, "data-in home screen-->" + model.getValue());

        if (model != null) {
            etxtAmount.setText(model.getValue());

            DENOM_CODE = model.getDenomCode();


            final float startSize = 52; // Size in pixels
            final float endSize = 14;
            final int animationDuration = 600; // Animation duration in ms

            ValueAnimator animator = ValueAnimator.ofFloat(startSize, endSize);
            animator.setDuration(animationDuration);

            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float animatedValue = (float) valueAnimator.getAnimatedValue();
                    etxtAmount.setTextSize(animatedValue);
                }
            });

            animator.start();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getProductsByService() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        RestAdapter rAdapter = new RestAdapter.Builder().setEndpoint(ServiceUrls.BASE_URL).
                setLogLevel(RestAdapter.LogLevel.BASIC).build();
        QDInterface restInt = rAdapter.create(QDInterface.class);
        rAdapter.getLogLevel();

        TypedInput tInput = ServiceRequest.returnTypedArr(this, ServiceMethods.GET_PRODUCTS_BY_VENDOR_ID,
                new Object[]{});

//        restInt.getProducts(SELECTED_SERVICE_PROVIDER, new Callback<Response>() {
//            @Override
//            public void success(Response result, Response response) {
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        progressDialog.dismiss();
//                    }
//                }, 2000);
//
//
//                //parse json objt
//                parseData(AppUtils.ConvertResponseToJSONArray(result));
//
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//                progressDialog.dismiss();
//
//                Logger.info(TAG, error.toString() + "--getCause--" + error.getCause() + "---getMessage--" + error.getMessage());
//            }
//        });


    }

    /**
     * @param mainArr
     */
    private void parseData(JSONArray mainArr) {

        Logger.info(TAG, "mainArr--->" + mainArr);

        JSONObject orgObjt = null;

        arrTotalPrdcts = new ArrayList<>();

        String plan_name;

        try {
            for (int i = 0; i < mainArr.length(); i++) {
                orgObjt = (JSONObject) mainArr.get(i);

                // Logger.info(TAG, "Service Name--->" + orgObjt.optString("name"));

            }
            JSONArray dealArr = orgObjt.getJSONArray("Products");

            for (int i = 0; i < dealArr.length(); i++) {
                JSONObject prodctsObjt = (JSONObject) dealArr.get(i);

                //Logger.info(TAG, "Pln Names--->" + prodctsObjt.optString("name"));

                plan_name = prodctsObjt.optString("name");

                JSONArray DenominationsArr = prodctsObjt.getJSONArray("Denominations");

                for (int j = 0; j < DenominationsArr.length(); j++) {

                    ProductsModel model = new ProductsModel();
                    JSONObject DenominationsArrObjt = (JSONObject) DenominationsArr.get(j);

                    //Logger.info(TAG, "DenominationsArrObjt--->" + DenominationsArrObjt.optString("name"));

                    model.setProductName(plan_name);

                    model.setDescription(DenominationsArrObjt.optString("Description"));
                    model.setOffer(DenominationsArrObjt.optString("Offer"));
                    model.setPlan_name(DenominationsArrObjt.optString("name"));
                    model.setTalktime(DenominationsArrObjt.optString("Talktime"));
                    model.setValidity(DenominationsArrObjt.optString("validity"));
                    model.setValue(DenominationsArrObjt.optString("value"));
                    model.setDenomCode(DenominationsArrObjt.optString("DenomCode"));

                    arrTotalPrdcts.add(model);

                }
            }

            Logger.info(TAG, "arrTotalPrdcts--->" + arrTotalPrdcts.size());

            Intent i = new Intent(AirTime.this, BrowsePlans.class);
            i.putExtra(AppConstants.PRODUCTS_DATA, arrTotalPrdcts);
            startActivityForResult(i, 121);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
