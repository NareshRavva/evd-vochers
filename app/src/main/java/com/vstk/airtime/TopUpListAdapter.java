/*
 * Copyright (C) 2015 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vstk.airtime;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.vstk.R;
import com.vstk.paymentScreen.PaymentDisplayActivity;
import com.vstk.services.DthListActivity;
import com.vstk.services.ServiceSelectionActivity;
import com.vstk.utils.AppConstants;

import java.util.ArrayList;


/**
 * @author Naresh R
 */
public class TopUpListAdapter extends RecyclerView.Adapter<TopUpListAdapter.ItemViewHolder> {

    ArrayList<TopUpModel> arrActivitiesData = new ArrayList<>();
    private Context context;
    public static boolean mIsLastItem = false;


    public TopUpListAdapter(Context context, ArrayList<TopUpModel> mItems) {
        this.context = context;
        this.arrActivitiesData = mItems;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.top_list_item, parent, false);
        if (context instanceof WifiListActivity || context instanceof DthListActivity) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dth_list_item, parent, false);
        }
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }


    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        mIsLastItem = loadData(position);
        final TopUpModel model = arrActivitiesData.get(position);

        holder.txtDealTitle.setText(model.getTitle());

        if (model.getMediaIcon() != null) {
            Picasso.with(context).invalidate(model.getMediaIcon().replace(" ", "%20"));
            Picasso.with(context)
                    .load(model.getMediaIcon().replace(" ", "%20"))
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(holder.imgMedia, null);
        }


        holder.llMainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (context instanceof ServiceSelectionActivity) {
//                    Intent ss = new Intent(context, PaymentDisplayActivity.class);
//                    context.startActivity(ss);
//                }else if (context instanceof TopUpListActivity || context instanceof WifiListActivity) {
//                    Intent ss = new Intent(context, ProductsList.class);
//                    ss.putExtra(AppConstants.VENDOR_ID, model.getId());
//                    context.startActivity(ss);
//                }

                Intent ss = new Intent(context, ProductsList.class);
                ss.putExtra(AppConstants.VENDOR_ID, model.getId());
                ss.putExtra(AppConstants.C_ID, model.getcID());
                ss.putExtra(AppConstants.IMG_BITMAP, model.getBitmap());
                context.startActivity(ss);

            }
        });

    }


    @Override
    public int getItemCount() {
        return arrActivitiesData.size();
    }

    /**
     * "handle" view that initiates a drag event when touched.
     */
    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        public final TextView txtDealTitle;
        public final ImageView imgMedia;
        public final LinearLayout llMainView;

        public ItemViewHolder(View itemView) {
            super(itemView);

            txtDealTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            imgMedia = (ImageView) itemView.findViewById(R.id.iv_image);
            llMainView = (LinearLayout) itemView.findViewById(R.id.ll_main_layout);
        }


    }

    public boolean loadData(int pos) {
        return pos == arrActivitiesData.size() - 1;
    }

}
