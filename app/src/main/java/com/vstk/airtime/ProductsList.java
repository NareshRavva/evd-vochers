package com.vstk.airtime;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import com.vstk.Api.QDInterface;
import com.vstk.Api.ServiceMethods;
import com.vstk.Api.ServiceRequest;
import com.vstk.Api.ServiceUrls;
import com.vstk.R;
import com.vstk.components.textview.TextView;
import com.vstk.logger.Logger;
import com.vstk.model.ProductsModel;
import com.vstk.printer.Activity_Main;
import com.vstk.utils.AppConstants;
import com.vstk.utils.AppUtils;
import com.vstk.utils.SharedPrefsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import HPRTAndroidSDK.HPRTPrinterHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

/**
 * Created by Naresh Ravva on 19/03/17.
 */

public class ProductsList extends Activity implements View.OnClickListener {

    private String TAG = "ProductsList";

    private ArrayList<ProductsModel> arrListProducts;

    private ProductsExpandableListAdapter expandableListAdapter;

    private HashMap<String, ArrayList<ProductsModel>> expandableListDetail = new HashMap<String, ArrayList<ProductsModel>>();

    private List<String> expandableListTitle;

    @BindView(R.id.expandableListView)
    ExpandableListView expandableListView;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);

        ButterKnife.bind(this);

        llBack.setOnClickListener(this);

        getProductsByService();

        Intent intent = getIntent();
        bitmap = (Bitmap) intent.getParcelableExtra(AppConstants.IMG_BITMAP);

        if (intent.getStringExtra(AppConstants.C_ID).equalsIgnoreCase("2")) {
            txtTitle.setText("ISP Operators");
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                /**
                 * OPEN BLUE TOOTH ADDRESS
                 */
                if (SharedPrefsUtils.getBluetoothAdres(ProductsList.this) != null)
                    HPRTPrinterHelper.PortOpen("Bluetooth," + SharedPrefsUtils.getBluetoothAdres(ProductsList.this));
            }
        }).start();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llBack:
                finish();
                break;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //CLOSE PORT
        HPRTPrinterHelper.PortClose();

    }

    private void getProductsByService() {

        AppUtils.showDialog(ProductsList.this);

        RestAdapter rAdapter = new RestAdapter.Builder().setEndpoint(ServiceUrls.BASE_URL).
                setLogLevel(RestAdapter.LogLevel.BASIC).build();
        QDInterface restInt = rAdapter.create(QDInterface.class);
        rAdapter.getLogLevel();

        TypedInput tInput = ServiceRequest.returnTypedArr(this, ServiceMethods.GET_PRODUCTS_BY_VENDOR_ID,
                new Object[]{});

        String vendorID = getIntent().getStringExtra(AppConstants.VENDOR_ID);
        String cID = getIntent().getStringExtra(AppConstants.C_ID);

        restInt.getProducts(cID, vendorID, "application/json", tInput, new Callback<Response>() {
            @Override
            public void success(Response result, Response response) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AppUtils.dismissDialog();
                    }
                }, 500);

                //parse json objt
                parseData(AppUtils.ConvertResponseToJSONArray(result));

            }

            @Override
            public void failure(RetrofitError error) {

                AppUtils.dismissDialog();

                Logger.info(TAG, error.toString() + "--getCause--" + error.getCause() + "---getMessage--" + error.getMessage());
            }
        });
    }

    /**
     * @param mainArr
     */
    private void parseData(JSONArray mainArr) {

        Logger.info(TAG, "mainArr--->" + mainArr);

        JSONObject orgObjt = null;

        arrListProducts = new ArrayList<>();

        String productName, productLogoPath, productID;

        try {
            for (int k = 0; k < mainArr.length(); k++) {
                orgObjt = (JSONObject) mainArr.get(k);

                String vendor_id, VendorLogoPath, vendorName;

                vendor_id = orgObjt.optString("VendorID");
                VendorLogoPath = orgObjt.optString("VendorLogoPath");
                vendorName = orgObjt.optString("name");

                JSONArray dealArr = orgObjt.getJSONArray("Products");

                for (int i = 0; i < dealArr.length(); i++) {
                    JSONObject prodctsObjt = (JSONObject) dealArr.get(i);

                    productName = prodctsObjt.optString("name");
                    productID = prodctsObjt.optString("ProductID");
                    productLogoPath = prodctsObjt.optString("productLogoPath");

                    JSONArray DenominationsArr = prodctsObjt.getJSONArray("Denominations");

                    for (int j = 0; j < DenominationsArr.length(); j++) {

                        ProductsModel model = new ProductsModel();
                        JSONObject DenominationsArrObjt = (JSONObject) DenominationsArr.get(j);

                        model.setProductName(productName);
                        model.setProductID(productID);
                        model.setProductLogoPath(productLogoPath);

                        model.setDescription(DenominationsArrObjt.optString("Description"));
                        model.setOffer(DenominationsArrObjt.optString("Offer"));
                        model.setPlan_name(DenominationsArrObjt.optString("name"));
                        model.setTalktime(DenominationsArrObjt.optString("Talktime"));
                        model.setValidity(DenominationsArrObjt.optString("validity"));
                        model.setValue(DenominationsArrObjt.optString("value"));
                        model.setDenomCode(DenominationsArrObjt.optString("DenomCode"));
                        model.setDisplyLogoPath(DenominationsArrObjt.optString("displyLogoPath"));

                        model.setVendor_id(vendor_id);
                        model.setVendorLogoPath(VendorLogoPath);
                        model.setVendorName(vendorName);

                        arrListProducts.add(model);

                    }
                }
            }

            Logger.info(TAG, "arrTotalPrdcts--->" + arrListProducts.size());

            addHeadersAndSubListItems();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     */
    private void addHeadersAndSubListItems() {

        /**
         * make headers in one array list
         */
        ArrayList<String> arrHeaders = new ArrayList<String>();

        for (int i = 0; i < arrListProducts.size(); i++) {
            ProductsModel bpProducts = arrListProducts.get(i);

            if (arrHeaders != null) {
                if (!arrHeaders.contains(bpProducts.getProductName())) {
                    arrHeaders.add(bpProducts.getProductName());
                }
            }
        }

        /**
         * add sublist to headers depends upon product type
         */
        ArrayList<ProductsModel> arrSubValues;

        for (int j = 0; j < arrHeaders.size(); j++) {
            arrSubValues = new ArrayList<ProductsModel>();

            for (int k = 0; k < arrListProducts.size(); k++) {
                if (arrHeaders.get(j) != null
                        && arrHeaders.get(j).toString().equalsIgnoreCase(arrListProducts.get(k).getProductName())) {

                    ProductsModel bpProducts = arrListProducts.get(k);

                    arrSubValues.add(bpProducts);

                    expandableListDetail.put(arrHeaders.get(j).toString(), arrSubValues);

                }
            }

        }

        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());

        expandableListAdapter = new ProductsExpandableListAdapter(ProductsList.this,
                expandableListTitle, expandableListDetail, expandableListView, arrListProducts, bitmap);
        expandableListView.setAdapter(expandableListAdapter);

        AppUtils.setListViewHeightBasedOnChildren(expandableListView);

    }
}
