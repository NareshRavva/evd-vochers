package com.vstk.airtime;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.vstk.Api.QDInterface;
import com.vstk.Api.ServiceMethods;
import com.vstk.Api.ServiceRequest;
import com.vstk.Api.ServiceUrls;
import com.vstk.R;
import com.vstk.components.textview.TextView;
import com.vstk.logger.Logger;
import com.vstk.utils.AppConstants;
import com.vstk.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

/**
 * Created by Naresh R on 11-01-2017.
 */

public class WifiListActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<TopUpModel> arrData = new ArrayList<>();

    private RecyclerView.LayoutManager mLayoutManager;

    private TopUpListAdapter adapter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.txtTitle)
    TextView txtTitle;

    @BindView(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private String TAG = "WifiListActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services_list);

        ButterKnife.bind(this);

        txtTitle.setText("ISP Operators");

        llBack.setOnClickListener(this);

        getVendorsList();

        // Configure the refreshing colors
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_blue_bright);


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getVendorsList();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llBack:
                finish();
                break;
        }

    }

    private void getVendorsList() {

        AppUtils.showDialog(this);

        RestAdapter rAdapter = new RestAdapter.Builder().setEndpoint(ServiceUrls.BASE_URL).
                setLogLevel(RestAdapter.LogLevel.BASIC).build();
        QDInterface restInt = rAdapter.create(QDInterface.class);
        rAdapter.getLogLevel();

        TypedInput tInput = ServiceRequest.returnTypedArr(this, ServiceMethods.GET_PRODUCTS_BY_VENDOR_ID,
                new Object[]{});

        String cID = getIntent().getStringExtra(AppConstants.C_ID);

        restInt.getVendorsList(cID, "application/json", tInput, new Callback<Response>() {
            @Override
            public void success(Response result, Response response) {

                //parse json objt
                parseData(AppUtils.ConvertResponseToJSONArray(result));

                mSwipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void failure(RetrofitError error) {

                AppUtils.dismissDialog();

                mSwipeRefreshLayout.setRefreshing(false);

                Logger.info(TAG, error.toString() + "--getCause--" + error.getCause() + "---getMessage--" + error.getMessage());
            }
        });
    }

    private void parseData(JSONArray jsonArray) {
        AppUtils.dismissDialog();

        arrData = new ArrayList<>();

        Logger.info(TAG, "response-->" + jsonArray);

        try {

            TopUpModel model;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject vendrObjt = (JSONObject) jsonArray.get(i);

                model = new TopUpModel();
                model.setTitle(vendrObjt.optString("Vendorname"));
                model.setId(vendrObjt.optString("VendorID"));
                model.setcID(AppConstants.CID_WIFI);
                model.setBitmap(urlToBitmap(vendrObjt.optString("PrintLogoURL")));
                model.setMediaIcon(vendrObjt.optString("DisplyLogoURL"));
                arrData.add(model);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        setDataToAdapter();
    }

    private Bitmap urlToBitmap(String printLogoURL) {
        Bitmap image = null;
        try {
            URL url = new URL(printLogoURL);
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            System.out.println(e);
        }
        return image;
    }

    /**
     *
     */
    private void setDataToAdapter() {
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new TopUpListAdapter(WifiListActivity.this, arrData);
        recyclerView.setAdapter(adapter);
    }
}
