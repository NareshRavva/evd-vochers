package com.vstk.airtime;

import java.io.Serializable;

/**
 * Created by Naresh Ravva on 21/03/17.
 */

public class PayMentResponseModel implements Serializable{

    String Expirydate,Pinnumber,RemainingBalance,SequenceNumber,SerialNumber,TransactionId,amount;

    String Footer,AdvertiseMent;

    public String getFooter() {
        return Footer;
    }

    public void setFooter(String footer) {
        Footer = footer;
    }

    public String getAdvertiseMent() {
        return AdvertiseMent;
    }

    public void setAdvertiseMent(String advertiseMent) {
        AdvertiseMent = advertiseMent;
    }

    public String getExpirydate() {
        return Expirydate;
    }

    public void setExpirydate(String expirydate) {
        Expirydate = expirydate;
    }

    public String getPinnumber() {
        return Pinnumber;
    }

    public void setPinnumber(String pinnumber) {
        Pinnumber = pinnumber;
    }

    public String getRemainingBalance() {
        return RemainingBalance;
    }

    public void setRemainingBalance(String remainingBalance) {
        RemainingBalance = remainingBalance;
    }

    public String getSequenceNumber() {
        return SequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        SequenceNumber = sequenceNumber;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
