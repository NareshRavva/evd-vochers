package com.vstk.airtime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.vstk.R;
import com.vstk.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nares on 25-02-2017.
 */
public class AirTimeHomeScreen extends AppCompatActivity {

    @BindView(R.id.llAirtime)
    LinearLayout llAirtime;

    @BindView(R.id.llIsp)
    LinearLayout llIsp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aitime_hm);

        ButterKnife.bind(this);

        llAirtime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent airtime = new Intent(AirTimeHomeScreen.this, TopUpListActivity.class);
                airtime.putExtra(AppConstants.C_ID, AppConstants.CID_TOPUP);
                startActivity(airtime);
            }
        });

        llIsp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent airtime = new Intent(AirTimeHomeScreen.this, WifiListActivity.class);
                airtime.putExtra(AppConstants.C_ID, AppConstants.CID_WIFI);
                startActivity(airtime);
            }
        });
    }
}
