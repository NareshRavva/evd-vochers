package com.vstk.airtime;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.vstk.Api.QDInterface;
import com.vstk.Api.ServiceMethods;
import com.vstk.Api.ServiceRequest;
import com.vstk.Api.ServiceUrls;
import com.vstk.R;
import com.vstk.logger.Logger;
import com.vstk.model.ProductsModel;
import com.vstk.printer.Activity_Main;
import com.vstk.utils.AppConstants;
import com.vstk.utils.AppUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

public class ProductsExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, ArrayList<ProductsModel>> expandableListDetail;
    private String TAG = "ProductsExpandableListAdapter";
    private ExpandableListView expandableListView;
    Bitmap bitmap;


    public ProductsExpandableListAdapter(Context context, List<String> expandableListTitle,
                                         HashMap<String, ArrayList<ProductsModel>> expandableListDetail,
                                         ExpandableListView expandableListView, ArrayList<ProductsModel> arrListProducts,
                                         Bitmap bitmap) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
        this.expandableListView = expandableListView;
        this.bitmap = bitmap;

        // Logger.info(TAG, "expandableListTitle size-->" +
        // expandableListTitle.size());
    }


    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(final int listPosition, final int expandedListPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        final ProductsModel bpProduct = (ProductsModel) getChild(listPosition, expandedListPosition);
        final String expandedListText = bpProduct.getProductName();

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.products_list_item, null);
        }
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.expandedListItem);
        ImageView imgProfile = (ImageView) convertView.findViewById(R.id.imgProfile);
        TextView txtProductType = (TextView) convertView.findViewById(R.id.txtProductType);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
        LinearLayout llMain = (LinearLayout) convertView.findViewById(R.id.llMain);

        llMain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder builder = AppUtils.getAlertDialog(context);
                builder.setMessage("Do u want to get the voucher for " + bpProduct.getVendorName() + " " + bpProduct.getValue())
                        .setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        // do final payment
                        doPayment(bpProduct);

                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
                final AlertDialog alert = builder.create();
                alert.show();


            }
        });

        String price = (bpProduct.getTalktime() != null) ? bpProduct.getTalktime() : null;
        expandedListTextView.setText("TalkTime : " + price);

        txtProductType.setText("Validity : " + bpProduct.getValidity() + " Days");

        tvPrice.setText(bpProduct.getValue());


        if (bpProduct.getDisplyLogoPath() != null) {
            //Picasso.with(context).invalidate(bpProduct.getDisplyLogoPath().replace(" ", "%20"));
            Picasso.with(context)
                    .load(bpProduct.getDisplyLogoPath().replace(" ", "%20"))
                    //.memoryPolicy(MemoryPolicy.NO_CACHE)
                    //.networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(imgProfile, null);
        }


        return convertView;
    }

    private void doPayment(ProductsModel bpProduct) {

        AppUtils.showDialog(context);

        RestAdapter rAdapter = new RestAdapter.Builder().setEndpoint(ServiceUrls.BASE_URL).
                setLogLevel(RestAdapter.LogLevel.BASIC).build();
        QDInterface restInt = rAdapter.create(QDInterface.class);
        rAdapter.getLogLevel();

        TypedInput tInput = ServiceRequest.returnTypedArr(context, ServiceMethods.DO_PAYMENY,
                new Object[]{
                        bpProduct.getVendor_id(),
                        bpProduct.getProductID(),
                        bpProduct.getDenomCode()
                });

        restInt.doPayment("application/json", tInput, new Callback<Response>() {
            @Override
            public void success(Response result, Response response) {

                //parse json objt
                parseData(AppUtils.ConvertResponseToJSONobjt(result));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AppUtils.dismissDialog();
                    }
                }, 1000);


            }

            @Override
            public void failure(RetrofitError error) {

                AppUtils.dismissDialog();

                Logger.info(TAG, error.toString() + "--getCause--" + error.getCause() + "---getMessage--" + error.getMessage());
            }
        });
    }

    private void parseData(JSONObject jsonObject) {

        Logger.info(TAG, "response--->" + jsonObject.toString());

        PayMentResponseModel prModel = new PayMentResponseModel();
        prModel.setExpirydate(jsonObject.optString("Expirydate"));
        prModel.setPinnumber(jsonObject.optString("Pinnumber"));
        prModel.setRemainingBalance(jsonObject.optString("RemainingBalance"));
        prModel.setSequenceNumber(jsonObject.optString("SequenceNumber"));
        prModel.setSerialNumber(jsonObject.optString("SerialNumber"));
        prModel.setTransactionId(jsonObject.optString("TransactionId"));
        prModel.setAmount(jsonObject.optString("amount"));
        prModel.setFooter(jsonObject.optString("Footer"));
        prModel.setAdvertiseMent(jsonObject.optString("AdvertiseMent"));

        Intent printScreen = new Intent(context, Activity_Main.class);
        printScreen.putExtra("PData", prModel);
        printScreen.putExtra(AppConstants.IMG_BITMAP, bitmap);
        context.startActivity(printScreen);


    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.products_group_list_item, null);
        }
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.listTitle);
        ImageView imgExpand = (ImageView) convertView.findViewById(R.id.imgExpand);
        RelativeLayout rlMainExpandItem = (RelativeLayout) convertView.findViewById(R.id.rlMainExpandItem);

        if (listPosition % 2 == 0) {
            rlMainExpandItem.setBackgroundColor(Color.parseColor("#F7F7F7"));

        } else {
            rlMainExpandItem.setBackgroundColor(Color.parseColor("#F1F1F1"));
        }


        ExpandableListView mExpandableListView = (ExpandableListView) parent;
        mExpandableListView.expandGroup(listPosition);


        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}