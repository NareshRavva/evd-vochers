package com.vstk.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.vstk.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit.client.Response;

/**
 * Created by Naresh R on 21-10-2016.
 */

public class AppUtils {
    protected static ProgressDialog progressDialog = null;


    /**
     * @param result
     * @return
     */
    public static JSONArray ConvertResponseToJSONArray(Response result) {

        JSONArray jObjt = null;
        //Try to get response body
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            jObjt = new JSONArray(sb.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        Logger.info("", "response--->" + jObjt.toString());

        return jObjt;
    }

    /**
     * @param result
     * @return
     */
    public static JSONObject ConvertResponseToJSONobjt(Response result) {

        JSONObject jObjt = null;
        //Try to get response body
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            jObjt = new JSONObject(sb.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        Logger.info("", "response--->" + jObjt.toString());

        return jObjt;
    }

    /**
     * show TOAST
     *
     * @param v
     * @param msg
     */
    public static void showToast(View v, Context ctx, String msg) {
        //Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();

        Snackbar snackbar = Snackbar.make(v, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    /**
     * set listview height properly in scrollView
     *
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        if (listView != null) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            for (int i = 0; i < listAdapter.getCount(); i++) {
                try {
                    View listItem = listAdapter.getView(i, null, listView);
                    listItem.measure(0, 0);
                    totalHeight += listItem.getMeasuredHeight();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
        }
    }

    /**
     * close keyboard
     *
     * @param myEditText
     */
    public static void hideKeyBoard(Context ctx, EditText myEditText) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
    }

    /**
     * show keyborad
     *
     * @param myEditText
     */
    public static void showKeyBoard(Context ctx, EditText myEditText) {

        InputMethodManager inputMethodManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(myEditText.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);

    }


    /**
     * show TOAST
     *
     * @param msg
     */
    public static void showToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Custom Toast for show msg's in entire application
     */
    public static void showAlert(Context ctx, String message) {
        // Create layout inflater object to inflate toast.xml file
        if (ctx != null) {

            try {
                final AlertDialog.Builder builder = AppUtils.getAlertDialog(ctx);
                builder.setMessage(message).setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                dialog.cancel();
                            }
                        });
                final AlertDialog alert = builder.create();

                alert.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#000000"));
                    }
                });
                alert.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * alert dialog
     *
     * @param ctx
     * @return
     */
    public static AlertDialog.Builder getAlertDialog(Context ctx) {

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            return new AlertDialog.Builder(ctx, R.style.AppCompatAlertDialogStyle);
        } else {
            return new AlertDialog.Builder(ctx);
        }

    }


    /**
     * show DAILOG
     *
     * @param ctx
     */
    public static void showDialog(Context ctx) {
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    /**
     * Dismiss DAILOG
     */
    public static void dismissDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public static String convertMilleTodate(String yourmilliseconds) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");

        Date resultdate = new Date(Long.parseLong(yourmilliseconds));

        return sdf.format(resultdate);
    }
}
