package com.vstk.utils;

/**
 * Created by Naresh Ravva on 21/03/17.
 */

public class AppConstants {

    /**
     * Shared Pref
     */
    public static final String VOCHERS_PREF = "VOCHERS_PREF";
    public static final String S_KEY_USER_NAME = "S_KEY_USER_NAME";
    public static final String S_KEY_PASSWRD = "S_KEY_PASSWRD";
    public static final String S_KEY_TERMINAL_ID = "S_KEY_TERMINAL_ID";
    public static final String S_KEY_AUTH_ID = "S_KEY_AUTH_ID";
    public static final String S_KEY_REMAIN_BALNC = "S_KEY_REMAIN_BALNC";
    public static final String S_KEY_RETAILOR_NAME = "S_KEY_RETAILOR_NAME";
    public static final String S_KEY_BLUE_TOOTH_ADDRS = "S_KEY_BLUE_TOOTH_ADDRS";


    public static final String VENDOR_ID = "vendor_id";
    public static final String C_ID = "c_id";
    public static final String IMG_BITMAP = "img_bitmap";


    /**
     * C id's
     */
    public static final String CID_TOPUP = "1";
    public static final String CID_WIFI = "2";
}
