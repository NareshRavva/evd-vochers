package com.vstk.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Naresh Ravva on 21/03/17.
 */

public class SharedPrefsUtils {

    /**
     * @param context
     * @param name
     */
    public static void setUserName(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.S_KEY_USER_NAME, name);
                editor.commit();
            }
        }
    }

    /**
     * @param context
     * @return
     */
    public static String getUserName(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
        String userId = appPref.getString(AppConstants.S_KEY_USER_NAME, null);
        return userId;
    }

    /**
     * @param context
     * @param name
     */
    public static void setUserPaswrd(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.S_KEY_PASSWRD, name);
                editor.commit();
            }
        }
    }

    /**
     * @param context
     * @return
     */
    public static String getUserPaswrd(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
        String userId = appPref.getString(AppConstants.S_KEY_PASSWRD, null);
        return userId;
    }

    /**
     * @param context
     * @param name
     */
    public static void setAuthID(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.S_KEY_AUTH_ID, name);
                editor.commit();
            }
        }
    }

    /**
     * @param context
     * @return
     */
    public static String getAuthID(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
        String userId = appPref.getString(AppConstants.S_KEY_AUTH_ID, null);
        return userId;
    }

    /**
     * @param context
     * @param name
     */
    public static void setTerminalID(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.S_KEY_TERMINAL_ID, name);
                editor.commit();
            }
        }
    }

    /**
     * @param context
     * @return
     */
    public static String getTerminalID(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
        String userId = appPref.getString(AppConstants.S_KEY_TERMINAL_ID, null);
        return userId;
    }

    /**
     * @param context
     * @param name
     */
    public static void setRemainBalnc(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.S_KEY_REMAIN_BALNC, name);
                editor.commit();
            }
        }
    }

    /**
     * @param context
     * @return
     */
    public static String getRemainBalnc(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
        String userId = appPref.getString(AppConstants.S_KEY_REMAIN_BALNC, null);
        return userId;
    }

    /**
     * @param context
     * @param name
     */
    public static void setRetailorName(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.S_KEY_RETAILOR_NAME, name);
                editor.commit();
            }
        }
    }

    /**
     * @param context
     * @return
     */
    public static String getRetailorName(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
        String userId = appPref.getString(AppConstants.S_KEY_RETAILOR_NAME, null);
        return userId;
    }

    /**
     * @param context
     * @param name
     */
    public static void setBluetoothAdres(Context context, String name) {
        if (context != null) {
            SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = appPref.edit();

            if (editor != null) {
                editor.putString(AppConstants.S_KEY_BLUE_TOOTH_ADDRS, name);
                editor.commit();
            }
        }
    }

    /**
     * @param context
     * @return
     */
    public static String getBluetoothAdres(Context context) {
        SharedPreferences appPref = context.getSharedPreferences(AppConstants.VOCHERS_PREF, Context.MODE_PRIVATE);
        String userId = appPref.getString(AppConstants.S_KEY_BLUE_TOOTH_ADDRS, null);
        return userId;
    }
}
