package com.vstk.homeScreen;

import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.vstk.R;
import com.vstk.accounts.AccountsActivity;
import com.vstk.airtime.AirTimeHomeScreen;
import com.vstk.airtime.TopUpListActivity;
import com.vstk.login.LoginActivity;
import com.vstk.scanTransfer.TransferActivity;
import com.vstk.services.ServicesActivity;
import com.vstk.utils.AppUtils;
import com.vstk.utils.SharedPrefsUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Naresh R on 09-01-2017.
 */

public class HomeScreen extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.left_drawer)
    ListView mDrawerList;

    @BindView(R.id.drawer)
    LinearLayout mDrawer;

    @BindView(R.id.llTransfer)
    LinearLayout llTransfer;

    @BindView(R.id.llAddFunds)
    LinearLayout llAddFunds;

    @BindView(R.id.llServices)
    LinearLayout llServices;

    @BindView(R.id.llAirtime)
    LinearLayout llAirtime;

    @BindView(R.id.llAccounts)
    LinearLayout llAccounts;

    @BindView(R.id.llHistory)
    LinearLayout llHistory;

    @BindView(R.id.ivMenu)
    ImageView ivMenu;

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.tvBalance)
    TextView tvBalance;

    @BindView(R.id.tvLoggedinAs)
    TextView tvLoggedinAs;


    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItemsModel> navDrawerItems;

    private NavDrawerListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        ButterKnife.bind(this);

        llTransfer.setOnClickListener(this);
        llAddFunds.setOnClickListener(this);
        llServices.setOnClickListener(this);
        llAirtime.setOnClickListener(this);
        llAccounts.setOnClickListener(this);
        llHistory.setOnClickListener(this);

        ivMenu.setOnClickListener(this);

        refreshDrawerList();

    }

    @Override
    protected void onResume() {

        tvName.setText(SharedPrefsUtils.getRetailorName(this));
        tvBalance.setText(SharedPrefsUtils.getRemainBalnc(this));
        super.onResume();
    }

    /**
     *
     */
    void refreshDrawerList() {
        /**
         * Update user profile pic
         */
        //setUsernameAndProfilePic();

        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        // nav drawer icons from resources
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Getting a reference to the slide bar drawer ( Title + ListView )
        mDrawer = (LinearLayout) findViewById(R.id.drawer);

        navDrawerItems = new ArrayList<NavDrawerItemsModel>();

        setDrawerItemsToList();

        tvLoggedinAs.setText(SharedPrefsUtils.getRetailorName(this));
    }

    /**
     * set drawer menus to list
     */
    private void setDrawerItemsToList() {
        // reg as bizz
        navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));

        navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));

        navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
        navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
        navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
        navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[8], navMenuIcons.getResourceId(8, -1)));
        //navDrawerItems.add(new NavDrawerItemsModel(navMenuTitles[9], navMenuIcons.getResourceId(9, -1)));
        navMenuIcons.recycle();

        //mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(this, navDrawerItems);
        mDrawerList.setAdapter(adapter);

        ValueAnimator anim = ValueAnimator.ofFloat(0, 150);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float slideOffset = (Float) valueAnimator.getAnimatedValue();
                //mDrawerToggle.onDrawerSlide(mDrawer, slideOffset);
            }
        });
        anim.setInterpolator(new DecelerateInterpolator());
        // You can change this duration to more closely match that of the
        // default animation.
        anim.setDuration(500);
        anim.start();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.llTransfer:
                Intent transeFer = new Intent(HomeScreen.this, TransferActivity.class);
                startActivity(transeFer);
                break;

            case R.id.llAddFunds:
                break;

            case R.id.llServices:
                Intent services = new Intent(HomeScreen.this, ServicesActivity.class);
                startActivity(services);
                break;

            case R.id.llAirtime:
                Intent airtime = new Intent(HomeScreen.this, AirTimeHomeScreen.class);
                startActivity(airtime);
                break;

            case R.id.llAccounts:
                Intent accounts = new Intent(HomeScreen.this, AccountsActivity.class);
                startActivity(accounts);
                break;

            case R.id.llHistory:
                break;

            case R.id.ivMenu:
                if (mDrawerLayout.isDrawerOpen(mDrawer)) {
                    mDrawerLayout.closeDrawer(mDrawer);
                } else {
                    mDrawerLayout.openDrawer(mDrawer);
                }
                break;
        }

    }

    /**
     * This list item click listener implements very simple view switching by changing the primary content text. The
     * drawer is closed when a selection is made.
     */
    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    mDrawerLayout.closeDrawer(mDrawer);
                    mDrawerList.setItemChecked(-1, true);

                    displayView(position);

                }
            }, 100);

            adapter.notifyDataSetChanged();
        }
    }

    /**
     * Displaying fragment view for selected nav drawer list item
     */
    private void displayView(int position) {

        String title = navDrawerItems.get(position).getTitle();

        if (title.equalsIgnoreCase("Logout")) {
            final AlertDialog.Builder builder = AppUtils.getAlertDialog(this);
            builder.setMessage("Are you sure you want to logout??")
                    .setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {

                    SharedPrefsUtils.setAuthID(HomeScreen.this, null);

                    Intent homeScreen = new Intent(HomeScreen.this, LoginActivity.class);
                    homeScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeScreen);
                    finish();

                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    dialog.cancel();
                }
            });
            final AlertDialog alert = builder.create();
            alert.show();


        }
    }

}
