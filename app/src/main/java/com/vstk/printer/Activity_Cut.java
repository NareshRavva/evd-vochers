package com.vstk.printer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import HPRTAndroidSDK.HPRTPrinterHelper;
import com.vstk.R;

public class Activity_Cut  extends Activity
{	
	private static HPRTPrinterHelper HPRTPrinter;
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);	   
		this.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.p_activity_cut);
						
	}
	
	public void onClickPartialCut(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	
    	try
    	{
    		HPRTPrinterHelper.PrintAndFeed(500);
    		HPRTPrinterHelper.CutPaper(HPRTPrinterHelper.HPRT_PARTIAL_CUT);
    	}
    	catch(Exception e)
    	{
    		
    	}
    }
	
	public void onClickPartialCutWithFeed(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	try
    	{
    		HPRTPrinterHelper.CutPaper(HPRTPrinterHelper.HPRT_PARTIAL_CUT_FEED, 240);
    	}
    	catch(Exception e)
    	{
    		
    	}
    }
}
