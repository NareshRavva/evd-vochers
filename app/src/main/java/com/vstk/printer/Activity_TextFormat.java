package com.vstk.printer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import HPRTAndroidSDK.HPRTPrinterHelper;
import HPRTAndroidSDK.PublicFunction;
import com.vstk.R;

public class Activity_TextFormat  extends Activity
{	
	private Context thisCon=null;
	private PublicFunction PFun=null;
	private EditText txtText=null;
	private EditText txtLeftMargin=null;
	private CheckBox chkDoubleWidth=null;
	private CheckBox chkDoubleHeight=null;
	private CheckBox chkUnderline=null;
	private CheckBox chkBold=null;
	private CheckBox chkMiniFont=null;
	private CheckBox chkTurnWhite=null;
	private RadioButton rdoLeft=null;
	private RadioButton rdoCenter=null;
	private RadioButton rdoRight=null;
	private Spinner spnHeighMultiple=null;
	private ArrayAdapter arrHeighMultiple;
	private Spinner spnWidthMultiple=null;
	private ArrayAdapter arrWidthMultiple;
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);	   
		this.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.p_activity_text_format);
		thisCon=this.getApplicationContext();
		
		txtText = (EditText) findViewById(R.id.txtText);
		txtLeftMargin = (EditText) findViewById(R.id.txtLeftMargin);
		chkDoubleWidth = (CheckBox) findViewById(R.id.chkDoubleWidth);
		chkDoubleHeight = (CheckBox) findViewById(R.id.chkDoubleHeight);
		chkUnderline = (CheckBox) findViewById(R.id.chkUnderline);
		chkBold = (CheckBox) findViewById(R.id.chkBold);
		chkMiniFont = (CheckBox) findViewById(R.id.chkMiniFont);
		chkTurnWhite = (CheckBox) findViewById(R.id.chkTurnWhite);
		rdoLeft=(RadioButton)this.findViewById(R.id.rdoLeft);
		rdoCenter=(RadioButton)this.findViewById(R.id.rdoCenter);
		rdoRight=(RadioButton)this.findViewById(R.id.rdoRight);
		
		spnHeighMultiple = (Spinner) findViewById(R.id.spnHeighMultiple);
		arrHeighMultiple = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item);
		arrHeighMultiple= ArrayAdapter.createFromResource(this, R.array.activity_text_format_multiple, android.R.layout.simple_spinner_item);
		arrHeighMultiple.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnHeighMultiple.setAdapter(arrHeighMultiple);
				
		spnWidthMultiple = (Spinner) findViewById(R.id.spnWidthMultiple);
		arrWidthMultiple = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item);
		arrWidthMultiple= ArrayAdapter.createFromResource(this, R.array.activity_text_format_multiple, android.R.layout.simple_spinner_item);
		arrWidthMultiple.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnWidthMultiple.setAdapter(arrWidthMultiple);			
	}
	
	public void onClickPrint(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	
    	try
    	{
    		String sText=txtText.getText().toString().trim();
	    	if(sText.length()==0)
	    	{
	    		Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_no_data), Toast.LENGTH_SHORT).show();
	    		return;
	    	}
	    	
	    	if(txtLeftMargin.getText().toString().trim().length()==0)
	    	{
	    		Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_no_data), Toast.LENGTH_SHORT).show();
	    		return;
	    	}
	    	else
	    	{
	    		int iLeftMargin= Integer.valueOf(txtLeftMargin.getText().toString().trim());
	    		if(iLeftMargin<0 || iLeftMargin>255)
	    		{
		    		Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_no_data), Toast.LENGTH_SHORT).show();
		    		return;
	    		}
	    	}
	    	
	    	int iAlignment=0;
	    	int iAttribute=0;
	    	int iTextSize=0;
	    	
	    	iAlignment=(rdoLeft.isChecked()?0:(rdoCenter.isChecked()?1:2));
	    	iAttribute=(chkDoubleHeight.isChecked()?16:0)
	    			| (chkDoubleWidth.isChecked()?32:0)
	    			| (chkUnderline.isChecked()?4:0)
	    			| (chkBold.isChecked()?2:0)
	    			| (chkMiniFont.isChecked()?1:0)
	    			| (chkTurnWhite.isChecked()?8:0);
	    	
	    	iTextSize=spnHeighMultiple.getSelectedItemPosition()+spnWidthMultiple.getSelectedItemPosition()*0x10;
	    	/*String sCodepage=PFun.ReadSharedPreferencesData("Codepage").split(",")[1];	
	    	String sCodeL="";
			HashMap<String,String> codeMap=PFun.getCodeLanguage();					 						
			if(codeMap.containsKey(sCodepage))					
				sCodeL=codeMap.get(sCodepage);	*/		
	    	//byte[] bData=sText.getBytes(sCodeL);
	    	PublicAction PAct=new PublicAction(thisCon);
	    	PAct.LanguageEncode();
	    	PAct.BeforePrintAction();
			Log.d("HPRTSDKSample","iAlignment-->"+iAlignment+"--iAttribute--"+iAttribute+"--iTextSize--"+iTextSize);

	    	HPRTPrinterHelper.PrintText(sText, iAlignment, iAttribute, iTextSize);
	    	PAct.AfterPrintAction();
    	}
		catch (Exception e)
		{			
			Log.d("HPRTSDKSample", (new StringBuilder("Activity_TextFormat --> onClickPrint ")).append(e.getMessage()).toString());
		}
    }
}
