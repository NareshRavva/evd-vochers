package com.vstk.printer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import HPRTAndroidSDK.HPRTPrinterHelper;
import HPRTAndroidSDK.PublicFunction;
import com.vstk.R;
import com.vstk.R;

public class Activity_PageMode  extends Activity
{	
	private Context thisCon=null;
	private PublicFunction PFun=null;
	
	private EditText edtText=null;
	private TextView txtLeftMargin=null;
	private TextView txtTopMargin=null;
	private EditText edtLeftMargin=null;
	private EditText edtTopMargin=null;
	private TextView txtWidth=null;
	private TextView txtHeight=null;
	private EditText edtWidth=null;
	private EditText edtHeight=null;
	private TextView txtHPosition=null;
	private TextView txtVPosition=null;
	private EditText edtHPosition=null;
	private EditText edtVPosition=null;
	private Spinner spnPrintDirection=null;
	private ArrayAdapter arrPrintDirection;
	
	private String sW="0";
	private String sH="0";
	private int iDirection=0;
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);	   
		this.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.p_activity_page_mode);
		thisCon=this.getApplicationContext();
		
		edtText = (EditText) findViewById(R.id.edtText);
		txtLeftMargin = (TextView) findViewById(R.id.txtLeftMargin);
		txtTopMargin = (TextView) findViewById(R.id.txtTopMargin);
		edtLeftMargin = (EditText) findViewById(R.id.edtLeftMargin);
		edtTopMargin = (EditText) findViewById(R.id.edtTopMargin);
		txtWidth = (TextView) findViewById(R.id.txtWidth);
		txtHeight = (TextView) findViewById(R.id.txtHeight);
		edtWidth = (EditText) findViewById(R.id.edtWidth);
		edtHeight = (EditText) findViewById(R.id.edtHeight);
		txtHPosition = (TextView) findViewById(R.id.txtHPosition);
		txtVPosition = (TextView) findViewById(R.id.txtVPosition);
		edtHPosition = (EditText) findViewById(R.id.edtHPosition);
		edtVPosition = (EditText) findViewById(R.id.edtVPosition);
		spnPrintDirection = (Spinner) findViewById(R.id.spnPrintDirection);
		arrPrintDirection = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item);
		arrPrintDirection= ArrayAdapter.createFromResource(this, R.array.activity_page_mode_print_direction_list, android.R.layout.simple_spinner_item);
		arrPrintDirection.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnPrintDirection.setAdapter(arrPrintDirection);
		spnPrintDirection.setOnItemSelectedListener(new OnItemSelectedPrintDirection());
		
		sW=this.getIntent().getStringExtra("PageModeW");
		sH=this.getIntent().getStringExtra("PageModeH");
		txtLeftMargin.setText(txtLeftMargin.getText().toString().replace("*", String.valueOf(PrinterProperty.PrintableWidth-1)));
		txtTopMargin.setText(txtTopMargin.getText().toString().replace("*", String.valueOf(Integer.valueOf(sH)-1)));
		txtWidth.setText(txtWidth.getText().toString().replace("*", String.valueOf(PrinterProperty.PrintableWidth)));
		txtHeight.setText(txtHeight.getText().toString().replace("*",sH));
	}
	
	private class OnItemSelectedPrintDirection implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{			
			iDirection=arg2;
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}
	
	public void onClickPrint(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	
    	try
    	{
    		String sText=edtText.getText().toString().trim();
	    	if(sText.length()==0)
	    	{
	    		Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_no_data), Toast.LENGTH_SHORT).show();
	    		return;
	    	}
	    	
    		if(Integer.valueOf(edtWidth.getText().toString())>PrinterProperty.PrintableWidth
    			|| Integer.valueOf(edtHeight.getText().toString())> Integer.valueOf(sH)
    			|| Integer.valueOf(edtLeftMargin.getText().toString())>PrinterProperty.PrintableWidth-1
    			|| Integer.valueOf(edtTopMargin.getText().toString())> Integer.valueOf(sH)-1)
    		{
    			Toast.makeText(thisCon, R.string.activity_page_mode_invalid_parameter, Toast.LENGTH_SHORT).show();
    			return;
    		}
    		
    		HPRTPrinterHelper.SelectPageMode();
    		HPRTPrinterHelper.SetPageModePrintArea(Integer.valueOf(edtLeftMargin.getText().toString()),
								    				Integer.valueOf(edtTopMargin.getText().toString()),
								    				Integer.valueOf(edtWidth.getText().toString()),
								    				Integer.valueOf(edtHeight.getText().toString()));
    		HPRTPrinterHelper.ClearPageModePrintAreaData();
    		HPRTPrinterHelper.SetPageModePrintDirection(iDirection);
    		HPRTPrinterHelper.SetPageModeAbsolutePosition(Integer.valueOf(edtHPosition.getText().toString()),
    													  Integer.valueOf(edtVPosition.getText().toString()));
    		HPRTPrinterHelper.PrintText(sText,0,0,0);
    		//HPRTPrinterHelper.PrintDataInPageMode();
    		HPRTPrinterHelper.PrintAndReturnStandardMode();
    	}
    	catch(Exception e)
    	{
    		Log.e("HPRTSDKSample", (new StringBuilder("Activity_PageMode --> onClickPrint ")).append(e.getMessage()).toString());
    	}
    }
}
