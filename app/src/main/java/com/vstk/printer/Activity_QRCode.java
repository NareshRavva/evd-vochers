package com.vstk.printer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;

import HPRTAndroidSDK.HPRTPrinterHelper;
import com.vstk.R;

public class Activity_QRCode  extends Activity
{	
	private Context thisCon=null;
	private Spinner spnQRCodeSize=null;
	private ArrayAdapter arrQRCodeSize;
	private Spinner spnQRCodeModel=null;
	private ArrayAdapter arrQRCodeModel;
	private Spinner spnQRCodeLevel=null;
	private ArrayAdapter arrQRCodeLevel;
	private EditText txtQRCodeData=null;
	private RadioButton rdoLeft=null;
	private RadioButton rdoCenter=null;
	private RadioButton rdoRight=null;
	private RadioGroup rdoGroup=null;
	private int justification=0;
	private int QRCodeSize=3;
	private int QRCodeModel=0;
	private int QRCodeLevel=0;
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);	   
		this.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.p_activity_qrcode);
		thisCon=this.getApplicationContext();
		
		txtQRCodeData=(EditText)this.findViewById(R.id.txtQRCodeData);
		rdoLeft=(RadioButton)this.findViewById(R.id.rdoLeft);
		rdoCenter=(RadioButton)this.findViewById(R.id.rdoCenter);
		rdoRight=(RadioButton)this.findViewById(R.id.rdoRight);
		rdoGroup=(RadioGroup)this.findViewById(R.id.radioGroup1);
		rdoGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId==rdoLeft.getId()) {
					justification=0;
				}else if (checkedId==rdoCenter.getId()) {
					justification=1;
				}else if (checkedId==rdoRight.getId()) {
					justification=2;
				}
			}
		});
		
		String[] sList;
		spnQRCodeSize = (Spinner) findViewById(R.id.spnQRCodeSize);
		sList="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16".split(",");		
		arrQRCodeSize = new ArrayAdapter<String>(Activity_QRCode.this,android.R.layout.simple_spinner_item, sList);
		arrQRCodeSize.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnQRCodeSize.setAdapter(arrQRCodeSize);
		spnQRCodeSize.setOnItemSelectedListener(new OnItemSelectedQRCodeSize());
		
		/*spnQRCodeModel = (Spinner) findViewById(R.id.spnQRCodeModel);	
		sList="1,2".split(",");		
		arrQRCodeModel = new ArrayAdapter<String>(Activity_QRCode.this,android.R.layout.simple_spinner_item, sList);
		arrQRCodeModel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnQRCodeModel.setAdapter(arrQRCodeModel);
		spnQRCodeModel.setOnItemSelectedListener(new OnItemSelectedQRCodeModel());*/
		
		spnQRCodeLevel = (Spinner) findViewById(R.id.spnQRCodeLevel);
		sList="L,M,Q,H".split(",");		
		arrQRCodeLevel = new ArrayAdapter<String>(Activity_QRCode.this,android.R.layout.simple_spinner_item, sList);
		arrQRCodeLevel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnQRCodeLevel.setAdapter(arrQRCodeLevel);
		spnQRCodeLevel.setOnItemSelectedListener(new OnItemSelectedQRCodeLevel());
	}

	private class OnItemSelectedQRCodeSize implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{			
			QRCodeSize=arg2;
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}
	
	private class OnItemSelectedQRCodeModel implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{			
			QRCodeModel=arg2;
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}
	
	private class OnItemSelectedQRCodeLevel implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{			
			QRCodeLevel=arg2;
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}
	
	public void onClickPrint(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	
    	try
    	{
	    	if(txtQRCodeData.getText().toString().trim().length()==0)
	    	{
	    		Toast.makeText(thisCon, getString(R.string.activity_qrcode_no_data), Toast.LENGTH_SHORT).show();
	    		return;
	    	}
	    	PublicAction PAct=new PublicAction(thisCon);
	    	PAct.BeforePrintAction();
	    	//HPRTPrinterHelper.SetJustification(justification+0x30);
//	    	HPRTPrinterHelper.PRTPrintBarcode2(txtQRCodeData.getText().toString(), QRCodeSize+1, QRCodeLevel+0x30,justification);
	    	HPRTPrinterHelper.PrintQRCode(txtQRCodeData.getText().toString(),(QRCodeSize+1),(QRCodeLevel+0x30),justification);
	    	PAct.AfterPrintAction();
    	}
		catch (Exception e)
		{			
			Log.d("HPRTSDKSample", (new StringBuilder("Activity_QRCode --> onClickPrint ")).append(e.getMessage()).toString());
		}
    }
}
