package com.vstk.printer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;

import com.vstk.R;

import HPRTAndroidSDK.HPRTPrinterHelper;

public class Activity_1DBarcodes  extends Activity
{	
	private Context thisCon=null;
	private Spinner spnBarcodeType=null;
	private ArrayAdapter arrBarcodeType;
	private Spinner spnBarcodeWidth=null;
	private ArrayAdapter arrBarcodeWidth;
	private Spinner spnBarcodeHRILayout=null;
	private ArrayAdapter arrBarcodeHRILayout;
	private EditText txtBarcodeData=null;
	private EditText txtBarcodeHeight=null;
	private RadioButton rdoLeft=null;
	private RadioButton rdoCenter=null;
	private RadioButton rdoRight=null;
	private RadioGroup rdoGroup=null;
	
	private int justification=0;
	private int BarcodeType=0;
	private int BarcodeWidth=2;
	private int BarcodeHRILayout=0;
		
	@SuppressWarnings("unchecked")
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);	   
		this.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.p_activity_1dbarcodes);
		thisCon=this.getApplicationContext();
				
		spnBarcodeType = (Spinner) findViewById(R.id.spnBarcodeType);
		//arrBarcodeType = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item);
		String[] barcode=new String[PrinterProperty.Barcode.split(",").length-1];
		if (PrinterProperty.Barcode.contains("QRCODE")) {
			for (int i = 0; i < PrinterProperty.Barcode.split(",").length-1; i++) {
				 barcode[i]=PrinterProperty.Barcode.split(",")[i];
			}
		}else {
			barcode=new String[PrinterProperty.Barcode.split(",").length];
			for (int i = 0; i < PrinterProperty.Barcode.split(",").length; i++) {
				 barcode[i]=PrinterProperty.Barcode.split(",")[i];
			}
		}
//		arrBarcodeType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,barcode);
		arrBarcodeType= ArrayAdapter.createFromResource(this, R.array.activity_1dbarcodes_barcode_type, android.R.layout.simple_spinner_item);
		arrBarcodeType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnBarcodeType.setAdapter(arrBarcodeType);
		spnBarcodeType.setOnItemSelectedListener(new OnItemSelectedBarcodeType());
		
		spnBarcodeWidth = (Spinner) findViewById(R.id.spnBarcodeWidth);
		arrBarcodeWidth = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item);
		arrBarcodeWidth= ArrayAdapter.createFromResource(this, R.array.activity_1dbarcodes_width, android.R.layout.simple_spinner_item);
		arrBarcodeWidth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnBarcodeWidth.setAdapter(arrBarcodeWidth);
		spnBarcodeWidth.setOnItemSelectedListener(new OnItemSelectedBarcodeWidth());
		
		spnBarcodeHRILayout = (Spinner) findViewById(R.id.spnBarcodeHRILayout);
		arrBarcodeHRILayout = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item);
		arrBarcodeHRILayout= ArrayAdapter.createFromResource(this, R.array.activity_1dbarcodes_hri_position, android.R.layout.simple_spinner_item);
		arrBarcodeHRILayout.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnBarcodeHRILayout.setAdapter(arrBarcodeHRILayout);	
		spnBarcodeHRILayout.setOnItemSelectedListener(new OnItemSelectedBarcodeHRILayout());
		
		txtBarcodeData=(EditText)this.findViewById(R.id.txtBarcodeData);
		txtBarcodeHeight=(EditText)this.findViewById(R.id.txtBarcodeHeight);
		rdoLeft=(RadioButton)this.findViewById(R.id.rdoLeft);
		rdoCenter=(RadioButton)this.findViewById(R.id.rdoCenter);
		rdoRight=(RadioButton)this.findViewById(R.id.rdoRight);
		rdoGroup=(RadioGroup)this.findViewById(R.id.radioGroup1);
		rdoGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId==rdoLeft.getId()) {
					justification=0;
					//Toast.makeText(thisCon, ""+justification, 1).show();
				}else if (checkedId==rdoCenter.getId()) {
					justification=1;
					//Toast.makeText(thisCon, ""+justification, 1).show();
				}else if (checkedId==rdoRight.getId()) {
					justification=2;
					//Toast.makeText(thisCon, ""+justification, 1).show();
				}
			}
		});
	}
	
	private class OnItemSelectedBarcodeType implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{						
//			InitDefaultData(spnBarcodeType.getSelectedItem().toString());
			BarcodeType=arg2;
			switch(BarcodeType)
			{
			case 0:
				txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_UPC_A));
				break;
			case 1:
				txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_UPC_E));
				break;
			case 2:
				txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_JAN13));
				break;
			case 3:
				txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_JAN8));
				break;
			case 4:
				txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_CODE39));
				break;
			case 5:
				txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_ITF));
				break;
			case 6:
				txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_CODABAR));
				break;
			case 7:
				txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_CODE93));
				break;
			case 8:
				txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_CODE128));
				break;
			}
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}
	
	private class OnItemSelectedBarcodeWidth implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{			
			BarcodeWidth= Integer.valueOf(arrBarcodeWidth.getItem(arg2).toString());
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}
	
	private class OnItemSelectedBarcodeHRILayout implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{			
			BarcodeHRILayout=arg2;
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}

	private void InitDefaultData(String strBarcodeType)
	{		
		ArrayAdapter arrABarcodeType;
		arrABarcodeType = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item);
		arrABarcodeType= ArrayAdapter.createFromResource(this, R.array.activity_1dbarcodes_barcode_type, android.R.layout.simple_spinner_item);
		for(int i=0;i<arrABarcodeType.getCount();i++)
		{
			if(arrABarcodeType.getItem(i).toString().equals(strBarcodeType))
			{
				BarcodeType=i;				
				break;
			}
		}
		
//		switch(BarcodeType)
//		{
//		case 0:
//			this.txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_UPC_A));
//			break;
//		case 1:
//			this.txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_UPC_E));
//			break;
//		case 2:
//			this.txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_JAN13));
//			break;
//		case 3:
//			this.txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_JAN8));
//			break;
//		case 4:
//			this.txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_CODE39));
//			break;
//		case 5:
//			this.txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_ITF));
//			break;
//		case 6:
//			this.txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_CODABAR));
//			break;
//		case 7:
//			this.txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_CODE93));
//			break;
//		case 8:
//			this.txtBarcodeData.setText(thisCon.getString(R.string.activity_1dbarcodes_CODE128));
//			break;
//		}
	}
	
	public void onClickPrint(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	
    	try
    	{
	    	if(txtBarcodeData.getText().toString().trim().length()==0)
	    	{
	    		Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_no_data), Toast.LENGTH_SHORT).show();
	    		return;
	    	}
	    	if (BarcodeType==0) {
				if (txtBarcodeData.getText().toString().trim().length()==11||txtBarcodeData.getText().toString().trim().length()==12) {
					
				}else {
					Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_data_error), Toast.LENGTH_SHORT).show();
		    		return;
				}
			}
	    	if (BarcodeType==1) {
	    		if (txtBarcodeData.getText().toString().trim().length()==11||txtBarcodeData.getText().toString().trim().length()==12) {
	    			
	    		}else {
	    			Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_data_error), Toast.LENGTH_SHORT).show();
	    			return;
	    		}
	    	}
	    	if (BarcodeType==3) {
	    		if (txtBarcodeData.getText().toString().trim().length()==7||txtBarcodeData.getText().toString().trim().length()==8) {
	    			
	    		}else {
	    			Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_data_error), Toast.LENGTH_SHORT).show();
	    			return;
	    		}
	    	}
	    	if (BarcodeType==2) {
	    		if (txtBarcodeData.getText().toString().trim().length()==12||txtBarcodeData.getText().toString().trim().length()==13) {
	    			
	    		}else {
	    			Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_data_error), Toast.LENGTH_SHORT).show();
	    			return;
	    		}
	    	}
	    	if (BarcodeType==4) {
	    		if (255>=txtBarcodeData.getText().toString().trim().length()&&txtBarcodeData.getText().toString().trim().length()>=1) {
	    			
	    		}else {
	    			Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_data_error), Toast.LENGTH_SHORT).show();
	    			return;
	    		}
	    	}
	    	if (BarcodeType==5) {
	    		if (254>=txtBarcodeData.getText().toString().trim().length()&&txtBarcodeData.getText().toString().trim().length()>=2) {
	    			
	    		}else {
	    			Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_data_error), Toast.LENGTH_SHORT).show();
	    			return;
	    		}
	    	}
	    	if (BarcodeType==6) {
	    		if (255>=txtBarcodeData.getText().toString().trim().length()&&txtBarcodeData.getText().toString().trim().length()>=2) {
	    			
	    		}else {
	    			Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_data_error), Toast.LENGTH_SHORT).show();
	    			return;
	    		}
	    	}
	    	if (BarcodeType==7) {
	    		if (255>=txtBarcodeData.getText().toString().trim().length()&&txtBarcodeData.getText().toString().trim().length()>=1) {
	    			
	    		}else {
	    			Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_data_error), Toast.LENGTH_SHORT).show();
	    			return;
	    		}
	    	}
	    	if (BarcodeType==8) {
	    		if (255>=txtBarcodeData.getText().toString().trim().length()&&txtBarcodeData.getText().toString().trim().length()>=2) {
	    			
	    		}else {
	    			Toast.makeText(thisCon, getString(R.string.activity_1dbarcodes_data_error), Toast.LENGTH_SHORT).show();
	    			return;
	    		}
	    	}
	    	PublicAction PAct=new PublicAction(thisCon);
	    	PAct.BeforePrintAction();
	    	HPRTPrinterHelper.PrintBarCode(BarcodeType+65, txtBarcodeData.getText().toString(), BarcodeWidth, Integer.valueOf(this.txtBarcodeHeight.getText().toString()), BarcodeHRILayout,justification);
	    	PAct.AfterPrintAction();
    	}
		catch (Exception e)
		{			
			Log.d("HPRTSDKSample", (new StringBuilder("Activity_1DBarcodes --> onClickPrint ")).append(e.getMessage()).toString());
		}
    }
}