package com.vstk.printer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import HPRTAndroidSDK.HPRTPrinterHelper;
import com.vstk.R;

public class Activity_Cashdrawer  extends Activity
{	
	private Context thisCon;
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);	   
		this.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.p_activity_cashdrawer);
	}

	public void onClickOpen1(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	
    	try
    	{	    	
    		int iRtn= HPRTPrinterHelper.OpenCashdrawer(0);
//    		Toast.makeText(this, ""+iRtn, 0).show();
    		if(iRtn==0)
    			Toast.makeText(thisCon, getString(R.string.activity_global_cmd_send), Toast.LENGTH_SHORT).show();
    	}
		catch (Exception e)
		{			
			Log.d("HPRTSDKSample", (new StringBuilder("Activity_Cashdrawer --> onClickOpen1 ")).append(e.getMessage()).toString());
		}
    }
	
	public void onClickOpen2(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	
    	try
    	{
    		int iRtn= HPRTPrinterHelper.OpenCashdrawer(1);
//    		Toast.makeText(this, ""+iRtn, 0).show();
	    	if(iRtn==0)
    			Toast.makeText(thisCon, getString(R.string.activity_global_cmd_send), Toast.LENGTH_SHORT).show();
    	}
		catch (Exception e)
		{			
			Log.d("HPRTSDKSample", (new StringBuilder("Activity_Cashdrawer --> onClickOpen2 ")).append(e.getMessage()).toString());
		}
    }
	
	public void onClickOpen12(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	
    	try
    	{
    		int iRtn= HPRTPrinterHelper.OpenCashdrawer(2);
//    		Toast.makeText(this, ""+iRtn, 0).show();
    		if(iRtn==0)
    			Toast.makeText(thisCon, getString(R.string.activity_global_cmd_send), Toast.LENGTH_SHORT).show();
    	}
		catch (Exception e)
		{			
			Log.d("HPRTSDKSample", (new StringBuilder("Activity_Cashdrawer --> onClickOpen12 ")).append(e.getMessage()).toString());
		}
    }
}
