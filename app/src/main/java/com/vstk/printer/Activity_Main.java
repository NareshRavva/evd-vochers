package com.vstk.printer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Pattern;

import HPRTAndroidSDK.HPRTPrinterHelper;
import HPRTAndroidSDK.IPort;
import HPRTAndroidSDK.PublicFunction;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.vstk.R;
import com.vstk.airtime.PayMentResponseModel;
import com.vstk.logger.Logger;
import com.vstk.utils.AppConstants;
import com.vstk.utils.AppUtils;
import com.vstk.utils.SharedPrefsUtils;

public class Activity_Main extends Activity {
    private Context thisCon = null;
    private BluetoothAdapter mBluetoothAdapter;
    private PublicFunction PFun = null;
    private PublicAction PAct = null;

    private Button btnWIFI = null;
    private Button btnBT = null;
    private Button btnUSB = null;

    private Spinner spnPrinterList = null;
    private TextView txtTips = null;
    private Button btnOpenCashDrawer = null;
    private Button btnSampleReceipt = null;
    private Button btn1DBarcodes = null;
    private Button btnQRCode = null;
    private Button btnPDF417 = null;
    private Button btnCut = null;
    private Button btnPageMode = null;
    private Button btnImageManage = null;
    private Button btnGetRemainingPower = null;

    private EditText edtTimes = null;

    private ArrayAdapter arrPrinterList;
    private static HPRTPrinterHelper HPRTPrinter = new HPRTPrinterHelper();
    private String ConnectType = "";
    private String PrinterName = "";
    private String PortParam = "";

    private UsbManager mUsbManager = null;
    private UsbDevice device = null;
    private static final String ACTION_USB_PERMISSION = "com.HPRTSDKSample";
    private PendingIntent mPermissionIntent = null;
    private static IPort Printer = null;

    private String TAG = "PRINT";

    PayMentResponseModel pModel;

    @BindView(R.id.txtDisplayPin)
    TextView txtDisplayPin;

    @BindView(R.id.iv_image)
    ImageView iv_image;

    String TOOTH_ADDRESS = "8C:DE:52:FA:6A:2F";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.p_activity_main);

        ButterKnife.bind(this);


        pModel = (PayMentResponseModel) getIntent().getSerializableExtra("PData");
        Logger.info(TAG, "p model response--->" + pModel.getExpirydate());
        displayPin(pModel.getPinnumber());


        try {
            thisCon = this.getApplicationContext();

            btnWIFI = (Button) findViewById(R.id.btnWIFI);
            btnUSB = (Button) findViewById(R.id.btnUSB);
            btnBT = (Button) findViewById(R.id.btnBT);

            //edtTimes = (EditText) findViewById(R.id.edtTimes);

            spnPrinterList = (Spinner) findViewById(R.id.spn_printer_list);
            txtTips = (TextView) findViewById(R.id.txtTips);
            btnSampleReceipt = (Button) findViewById(R.id.btnSampleReceipt);
            btnOpenCashDrawer = (Button) findViewById(R.id.btnOpenCashDrawer);
            btn1DBarcodes = (Button) findViewById(R.id.btn1DBarcodes);
            btnQRCode = (Button) findViewById(R.id.btnQRCode);
            btnPDF417 = (Button) findViewById(R.id.btnPDF417);
            btnCut = (Button) findViewById(R.id.btnCut);
            btnPageMode = (Button) findViewById(R.id.btnPageMode);
            btnImageManage = (Button) findViewById(R.id.btnImageManage);
            btnGetRemainingPower = (Button) findViewById(R.id.btnGetRemainingPower);

            mPermissionIntent = PendingIntent.getBroadcast(thisCon, 0, new Intent(ACTION_USB_PERMISSION), 0);
            IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
            thisCon.registerReceiver(mUsbReceiver, filter);

            PFun = new PublicFunction(thisCon);
            PAct = new PublicAction(thisCon);
            InitSetting();
            InitCombox();
            this.spnPrinterList.setOnItemSelectedListener(new OnItemSelectedPrinter());
            //Enable Bluetooth
            EnableBluetooth();
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    // TODO Auto-generated method stub
                    super.handleMessage(msg);
                    if (msg.what == 1) {
                        Toast.makeText(thisCon, "succeed", 0).show();
                        dialog.cancel();
                    } else {
                        Toast.makeText(thisCon, "failure", 0).show();
                        dialog.cancel();
                    }
                }
            };
        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> onCreate ")).append(e.getMessage()).toString());
        }

        if (HPRTPrinterHelper.IsOpened()) {
            txtTips.setText(thisCon.getString(R.string.activity_main_connected));
        }
    }

    private void displayPin(String pinnumber) {

//        try {
//            Intent intent = getIntent();
//            Bitmap bitmap = (Bitmap) intent.getParcelableExtra(AppConstants.IMG_BITMAP);
//            iv_image.setImageBitmap(bitmap);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        if (pModel.getPinnumber().contains("|")) {
            String[] pinNumber = pModel.getPinnumber().split(Pattern.quote("|"));
            String username = pinNumber[0];
            String paswrd = pinNumber[1];

            String newUsername = username.substring(username.length() - 4);
            String newPasWrd = paswrd.substring(paswrd.length() - 4);

            String wifiVochr = "USER NAME: " + "XXXX" + newUsername + "\n" + "PASSWORD: " + "XX" + newPasWrd;

            txtDisplayPin.setText(wifiVochr);

        } else {
            String newpinnumber = pinnumber.substring(pinnumber.length() - 4);
            txtDisplayPin.setText("XXXX XXXX XXXX " + newpinnumber);
        }

    }

    private void InitSetting() {
        String SettingValue = "";
        SettingValue = PFun.ReadSharedPreferencesData("Codepage");
        if (SettingValue.equals(""))
            PFun.WriteSharedPreferencesData("Codepage", "0,PC437(USA:Standard Europe)");

        SettingValue = PFun.ReadSharedPreferencesData("Cut");
        if (SettingValue.equals(""))
            PFun.WriteSharedPreferencesData("Cut", "0");    //0:��ֹ,1:��ӡǰ,2:��ӡ��

        SettingValue = PFun.ReadSharedPreferencesData("Cashdrawer");
        if (SettingValue.equals(""))
            PFun.WriteSharedPreferencesData("Cashdrawer", "0");

        SettingValue = PFun.ReadSharedPreferencesData("Buzzer");
        if (SettingValue.equals(""))
            PFun.WriteSharedPreferencesData("Buzzer", "0");

        SettingValue = PFun.ReadSharedPreferencesData("Feeds");
        if (SettingValue.equals(""))
            PFun.WriteSharedPreferencesData("Feeds", "0");
    }

    //add printer list
    private void InitCombox() {
        try {
            arrPrinterList = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
            String strSDKType = thisCon.getString(R.string.sdk_type);
            if (strSDKType.equals("all"))
                arrPrinterList = ArrayAdapter.createFromResource(this, R.array.printer_list_all, android.R.layout.simple_spinner_item);
            if (strSDKType.equals("hprt"))
                arrPrinterList = ArrayAdapter.createFromResource(this, R.array.printer_list_hprt, android.R.layout.simple_spinner_item);
            if (strSDKType.equals("mkt"))
                arrPrinterList = ArrayAdapter.createFromResource(this, R.array.printer_list_mkt, android.R.layout.simple_spinner_item);
            if (strSDKType.equals("mprint"))
                arrPrinterList = ArrayAdapter.createFromResource(this, R.array.printer_list_mprint, android.R.layout.simple_spinner_item);
            if (strSDKType.equals("sycrown"))
                arrPrinterList = ArrayAdapter.createFromResource(this, R.array.printer_list_sycrown, android.R.layout.simple_spinner_item);
            if (strSDKType.equals("mgpos"))
                arrPrinterList = ArrayAdapter.createFromResource(this, R.array.printer_list_mgpos, android.R.layout.simple_spinner_item);
            if (strSDKType.equals("ds"))
                arrPrinterList = ArrayAdapter.createFromResource(this, R.array.printer_list_ds, android.R.layout.simple_spinner_item);
            if (strSDKType.equals("cst"))
                arrPrinterList = ArrayAdapter.createFromResource(this, R.array.printer_list_cst, android.R.layout.simple_spinner_item);
            if (strSDKType.equals("other"))
                arrPrinterList = ArrayAdapter.createFromResource(this, R.array.printer_list_other, android.R.layout.simple_spinner_item);
            arrPrinterList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            PrinterName = arrPrinterList.getItem(0).toString();
            spnPrinterList.setAdapter(arrPrinterList);
        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> InitCombox ")).append(e.getMessage()).toString());
        }
    }

    private class OnItemSelectedPrinter implements OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            PrinterName = arrPrinterList.getItem(arg2).toString();
            HPRTPrinter = new HPRTPrinterHelper(thisCon, PrinterName);
            CapturePrinterFunction();
            GetPrinterProperty();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }
    }

    //EnableBluetooth
    private boolean EnableBluetooth() {
        boolean bRet = false;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null) {
            if (mBluetoothAdapter.isEnabled())
                return true;
            mBluetoothAdapter.enable();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!mBluetoothAdapter.isEnabled()) {
                bRet = true;
                Log.d("PRTLIB", "BTO_EnableBluetooth --> Open OK");
            }
        } else {
            Log.d("HPRTSDKSample", (new StringBuilder("Activity_Main --> EnableBluetooth ").append("Bluetooth Adapter is null.")).toString());
        }
        return bRet;
    }

    //call back by scan bluetooth printer
    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        try {
            String strIsConnected;
            switch (resultCode) {
                case HPRTPrinterHelper.ACTIVITY_CONNECT_BT:
                    String strBTAddress = "";
                    strIsConnected = data.getExtras().getString("is_connected");
                    if (strIsConnected.equals("NO")) {
                        txtTips.setText(thisCon.getString(R.string.activity_main_scan_error));
                        return;
                    } else {
                        txtTips.setText(thisCon.getString(R.string.activity_main_connected));
                        return;
                    }
                case HPRTPrinterHelper.ACTIVITY_CONNECT_WIFI:
                    String strIPAddress = "";
                    String strPort = "";
                    strIsConnected = data.getExtras().getString("is_connected");
                    if (strIsConnected.equals("NO")) {
                        txtTips.setText(thisCon.getString(R.string.activity_main_scan_error));
                        return;
                    } else {
                        strIPAddress = data.getExtras().getString("IPAddress");
                        strPort = data.getExtras().getString("Port");
                        if (strIPAddress == null || !strIPAddress.contains("."))
                            return;
                        HPRTPrinter = new HPRTPrinterHelper(thisCon, spnPrinterList.getSelectedItem().toString().trim());
                        if (HPRTPrinterHelper.PortOpen("WiFi," + strIPAddress + "," + strPort) != 0)
                            txtTips.setText(thisCon.getString(R.string.activity_main_connecterr));
                        else
                            txtTips.setText(thisCon.getString(R.string.activity_main_connected));
                        return;
                    }
                case HPRTPrinterHelper.ACTIVITY_IMAGE_FILE:
                    dialog = new ProgressDialog(Activity_Main.this);
                    dialog.setMessage("Printing.....");
                    dialog.setProgress(100);
                    dialog.show();
                    new Thread() {
                        public void run() {
                            PAct.BeforePrintAction();
                            String strImageFile = data.getExtras().getString("FilePath");
                            int printImage;
                            try {
                                printImage = HPRTPrinterHelper.PrintImage(strImageFile, (byte) 0, (byte) 0);
                                if (printImage > 0) {
                                    handler.sendEmptyMessage(1);
                                } else {
                                    handler.sendEmptyMessage(0);
                                }
                            } catch (Exception e) {
                                handler.sendEmptyMessage(0);
                            }
                            PAct.AfterPrintAction();
                        }

                        ;
                    }.start();
                    return;
                case HPRTPrinterHelper.ACTIVITY_PRNFILE:
                    PAct.LanguageEncode();
                    PAct.BeforePrintAction();
                    String strPRNFile = data.getExtras().getString("FilePath");
                    System.out.println(strPRNFile);
                    HPRTPrinterHelper.PrintBinaryFile(strPRNFile);
                    PAct.AfterPrintAction();

	  				/*String strPRNFile=data.getExtras().getString("FilePath");	  					  				
                      byte[] bR=new byte[1];
	  				byte[] bW=new byte[3];
	  				bW[0]=0x10;bW[1]=0x04;bW[2]=0x02;
	  				for(int i=0;i<Integer.parseInt(edtTimes.getText().toString());i++)
	  				{
	  					HPRTPrinterHelper.PrintBinaryFile(strPRNFile);
	  					HPRTPrinterHelper.DirectIO(bW, null, 0);
	  					HPRTPrinterHelper.DirectIO(null, bR, 1);	  						
	  				}*/
                    return;
            }
        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> onActivityResult ")).append(e.getMessage()).toString());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressLint("NewApi")
    public void onClickConnect(View view) {
        if (!checkClick.isClickEvent()) return;

        try {
            if (HPRTPrinter != null) {
                // HPRTPrinterHelper.PortClose();
            }

            if (view.getId() == R.id.btnBT) {
                ConnectType = "Bluetooth";
                Intent serverIntent = new Intent(thisCon, Activity_DeviceList.class);
                startActivityForResult(serverIntent, HPRTPrinterHelper.ACTIVITY_CONNECT_BT);
                return;
            } else if (view.getId() == R.id.btnWIFI) {
                ConnectType = "WiFi";
                Intent serverIntent = new Intent(thisCon, Activity_Wifi.class);
                serverIntent.putExtra("PN", PrinterName);
                startActivityForResult(serverIntent, HPRTPrinterHelper.ACTIVITY_CONNECT_WIFI);
                return;
            } else if (view.getId() == R.id.btnUSB) {
                ConnectType = "USB";
                HPRTPrinter = new HPRTPrinterHelper(thisCon, arrPrinterList.getItem(spnPrinterList.getSelectedItemPosition()).toString());
                //USB not need call "iniPort"
                mUsbManager = (UsbManager) thisCon.getSystemService(Context.USB_SERVICE);
                HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
                Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();

                boolean HavePrinter = false;
                while (deviceIterator.hasNext()) {
                    device = deviceIterator.next();
                    int count = device.getInterfaceCount();
                    for (int i = 0; i < count; i++) {
                        UsbInterface intf = device.getInterface(i);
                        //Class IDΪ7��ʾ��USB�豸Ϊ��ӡ���豸
                        if (intf.getInterfaceClass() == 7) {
                            HavePrinter = true;
                            mUsbManager.requestPermission(device, mPermissionIntent);
                        }
                    }
                }
                if (!HavePrinter)
                    txtTips.setText(thisCon.getString(R.string.activity_main_connect_usb_printer));
            }
        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> onClickConnect " + ConnectType)).append(e.getMessage()).toString());
        }
    }

    private BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                String action = intent.getAction();
                //Toast.makeText(thisCon, "now:"+System.currentTimeMillis(), Toast.LENGTH_LONG).show();
                //HPRTPrinterHelper.WriteLog("1.txt", "fds");
                //��ȡ����USB�豸Ȩ��
                if (ACTION_USB_PERMISSION.equals(action)) {
                    synchronized (this) {
                        device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            if (HPRTPrinterHelper.PortOpen(device) != 0) {
                                HPRTPrinter = null;
                                txtTips.setText(thisCon.getString(R.string.activity_main_connecterr));
                                return;
                            } else
                                txtTips.setText(thisCon.getString(R.string.activity_main_connected));

                        } else {
                            return;
                        }
                    }
                }
                if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                    device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (device != null) {
                        //HPRTPrinterHelper.PortClose();
                    }
                }
            } catch (Exception e) {
                Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> mUsbReceiver ")).append(e.getMessage()).toString());
            }
        }
    };
    private Handler handler;
    private ProgressDialog dialog;

    public void onClickClose(View view) {
        if (!checkClick.isClickEvent()) return;

        try {
            if (HPRTPrinter != null) {
                // HPRTPrinterHelper.PortClose();
            }
            this.txtTips.setText(R.string.activity_main_tips);
            return;
        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> onClickClose ")).append(e.getMessage()).toString());
        }
    }

    public void onClickbtnSetting(View view) {
        if (!checkClick.isClickEvent()) return;

        try {
            startActivity(new Intent(Activity_Main.this, Activity_Setting.class));
        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> onClickClose ")).append(e.getMessage()).toString());
        }
    }

    public void onClickDo(View view) {
        if (!checkClick.isClickEvent()) return;

        if (!HPRTPrinterHelper.IsOpened()) {
            Toast.makeText(thisCon, thisCon.getText(R.string.activity_main_tips), Toast.LENGTH_SHORT).show();
            return;
        }

        if (view.getId() == R.id.btnGetStatus) {
            Intent myIntent = new Intent(this, Activity_Status.class);
            myIntent.putExtra("StatusMode", PrinterProperty.StatusMode);
            startActivityFromChild(this, myIntent, 0);
        } else if (view.getId() == R.id.btnOpenCashDrawer) {
            Intent myIntent = new Intent(this, Activity_Cashdrawer.class);
            startActivityFromChild(this, myIntent, 0);
        } else if (view.getId() == R.id.btnSampleReceipt) {
            PrintSampleReceipt();
        } else if (view.getId() == R.id.btn1DBarcodes) {
            Intent myIntent = new Intent(this, Activity_1DBarcodes.class);
            startActivityFromChild(this, myIntent, 0);
        } else if (view.getId() == R.id.btnCut) {
            Intent myIntent = new Intent(this, Activity_Cut.class);
            startActivityFromChild(this, myIntent, 0);
        } else if (view.getId() == R.id.btnTextFormat) {
            Intent myIntent = new Intent(this, Activity_TextFormat.class);
            startActivityFromChild(this, myIntent, 0);
        } else if (view.getId() == R.id.btnPrintImageFile) {
            Intent myIntent = new Intent(this, Activity_PRNFile.class);
            myIntent.putExtra("Folder", android.os.Environment.getExternalStorageDirectory().getAbsolutePath());
            myIntent.putExtra("FileFilter", "jpg,gif,png,bmp,");
            startActivityForResult(myIntent, HPRTPrinterHelper.ACTIVITY_IMAGE_FILE);
        } else if (view.getId() == R.id.btnPrintPRNFile) {
            Intent myIntent = new Intent(this, Activity_PRNFile.class);
            myIntent.putExtra("Folder", android.os.Environment.getExternalStorageDirectory().getAbsolutePath());
            myIntent.putExtra("FileFilter", "prn,");
            startActivityForResult(myIntent, HPRTPrinterHelper.ACTIVITY_PRNFILE);
        } else if (view.getId() == R.id.btnPageMode) {
            String[] sArrXY;
            sArrXY = PrinterProperty.PagemodeArea.split(",");
            Intent myIntent = new Intent(this, Activity_PageMode.class);
            myIntent.putExtra("PageModeW", sArrXY[0]);
            myIntent.putExtra("PageModeH", sArrXY[1]);
            startActivityFromChild(this, myIntent, 0);
        } else if (view.getId() == R.id.btnQRCode) {
            Intent myIntent = new Intent(this, Activity_QRCode.class);
            startActivityFromChild(this, myIntent, 0);
        } else if (view.getId() == R.id.btnPDF417) {
            Intent myIntent = new Intent(this, Activity_PDF417.class);
            startActivityFromChild(this, myIntent, 0);
        } else if (view.getId() == R.id.btnImageManage) {
            Intent myIntent = new Intent(this, Activity_Image_Manage.class);
            startActivityFromChild(this, myIntent, 0);
        } else if (view.getId() == R.id.btnPrintTestPage) {
            printVocher();
        }
    }

    public void printVocher() {
        try {
            PAct.LanguageEncode();
            PAct.BeforePrintAction();

            Intent intent = getIntent();
            Bitmap bitmap = (Bitmap) intent.getParcelableExtra(AppConstants.IMG_BITMAP);

//    		String strPrintText="HPRT SDK Sample!";
//    		HPRTPrinterHelper.PrintText(thisCon.getString(R.string.activity_main_originalsize) + strPrintText+"\n",0,0,0);
//			HPRTPrinterHelper.PrintText(thisCon.getString(R.string.activity_main_heightsize) + strPrintText+"\n",0,16,0);
//			HPRTPrinterHelper.PrintText(thisCon.getString(R.string.activity_main_widthsize) + strPrintText+"\n",0,32,0);
//			HPRTPrinterHelper.PrintText(thisCon.getString(R.string.activity_main_heightwidthsize) + strPrintText+"\n",0,48,0);
//			HPRTPrinterHelper.PrintText(thisCon.getString(R.string.activity_main_bold) + strPrintText+"\n",0,2,0);
//			HPRTPrinterHelper.PrintText(thisCon.getString(R.string.activity_main_underline) + strPrintText+"\n",0,4,0);
//			HPRTPrinterHelper.PrintText(thisCon.getString(R.string.activity_main_minifront) + strPrintText+"\n",0,1,0);
//			HPRTPrinterHelper.Initialize();


            Bitmap icon = BitmapFactory.decodeResource(getResources(),
                    R.drawable.logo_vodacom);
            int padding = 130;
            if (pModel.getPinnumber().contains("|")) {
                padding = 70;
            }

            HPRTPrinterHelper.PrintBitmap(pad(bitmap, padding, 0), (byte) 0, (byte) 0);

            //HPRTPrinterHelper.PrintText("\n" + "1234567891" + "\n", 0, 2, 0);

            String terminalID = SharedPrefsUtils.getTerminalID(Activity_Main.this);
            HPRTPrinterHelper.PrintText("\n" + "Terminal ID : " + terminalID + "", 0, 2, 0);

            String transactionID = pModel.getTransactionId();
            HPRTPrinterHelper.PrintText("TransactionID:" + transactionID + "", 0, 2, 0);

            String date = convertMilleTodate("" + System.currentTimeMillis());
            String time = convertMilleToTime("" + System.currentTimeMillis());

            HPRTPrinterHelper.PrintText("Date:" + date + "   " + "Time:" + time + "\n", 0, 2, 0);

            String amount = pModel.getAmount();
            HPRTPrinterHelper.PrintText("RECHARGE AMOUNT : " + amount + "", 1, 16, 0);

            HPRTPrinterHelper.PrintText("--------------------------------", 0, 2, 0);

            if (pModel.getPinnumber().contains("|")) {
                String[] pinNumber = pModel.getPinnumber().split(Pattern.quote("|"));
                String username = pinNumber[0];
                String paswrd = pinNumber[1];

                HPRTPrinterHelper.PrintText("UN: " + username, 1, 48, 0);
                //HPRTPrinterHelper.PrintText(username, 1, 48, 0);
                HPRTPrinterHelper.PrintText("PW: " + paswrd + "\n", 1, 48, 0);
                //HPRTPrinterHelper.PrintText(paswrd, 1, 48, 0);


            } else {
                HPRTPrinterHelper.PrintText("PIN NUMBER", 1, 48, 0);

                StringBuilder str = new StringBuilder(pModel.getPinnumber());
                int idx = str.length() - 4;

                while (idx > 0) {
                    str.insert(idx, "");
                    idx = idx - 4;
                }

                System.out.println(str.toString());
                HPRTPrinterHelper.PrintText(str.toString() + "\n", 0, 48, 0);

            }

            String expiryDate = pModel.getExpirydate();
            HPRTPrinterHelper.PrintText("Expiry Date:" + expiryDate + "", 0, 2, 0);

            String Serialno = pModel.getSerialNumber();
            HPRTPrinterHelper.PrintText("Serial No : " + Serialno + "", 0, 2, 0);

            HPRTPrinterHelper.PrintText("--------------------------------", 0, 2, 0);

            HPRTPrinterHelper.PrintText("Recharge Instructions:", 0, 2, 0);
            HPRTPrinterHelper.PrintText(pModel.getFooter() + "\n", 0, 2, 0);

            HPRTPrinterHelper.PrintText(pModel.getAdvertiseMent() + "\n", 0, 2, 0);

            HPRTPrinterHelper.PrintText("Thanks for using our service", 1, 2, 0);

            HPRTPrinterHelper.PrintText("------ ** End of Copy ** -------", 0, 2, 0);


            HPRTPrinterHelper.PrintText("powered by innovGraph" + "\n", 2, 2, 0);

            //HPRTPrinterHelper.CutPaper(HPRTPrinterHelper.HPRT_PARTIAL_CUT_FEED, 240);
            //HPRTPrinterHelper.PrintAndFeed(500);
            //HPRTPrinterHelper.CutPaper(HPRTPrinterHelper.HPRT_PARTIAL_CUT);

            HPRTPrinterHelper.Initialize();
            //PRT.PRTReset();
            PAct.AfterPrintAction();

            movetoVendorsPage();

        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> onClickWIFI ")).append(e.getMessage()).toString());
        }
    }

    private void movetoVendorsPage() {
        finish();
    }

    public static String convertMilleTodate(String yourmilliseconds) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        Date resultdate = new Date(Long.parseLong(yourmilliseconds));

        return sdf.format(resultdate);
    }

    public static String convertMilleToTime(String yourmilliseconds) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Date resultdate = new Date(Long.parseLong(yourmilliseconds));

        return sdf.format(resultdate);
    }

    public static Bitmap createBlackAndWhite(Bitmap src) {
        int width = src.getWidth();
        int height = src.getHeight();
        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(width, height, src.getConfig());
        // color information
        int A, R, G, B;
        int pixel;

        // scan through all pixels
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                // get pixel color
                pixel = src.getPixel(x, y);
                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);
                int gray = (int) (0.2989 * R + 0.5870 * G + 0.1140 * B);

                // use 128 as threshold, above -> white, below -> black
                if (gray > 128)
                    gray = 255;
                else
                    gray = 0;
                // set new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, gray, gray, gray));
            }
        }
        return bmOut;
    }


    public Bitmap pad(Bitmap Src, int padding_x, int padding_y) {
        int color = getResources().getColor(R.color.white);
        Bitmap outputimage = Bitmap.createBitmap(Src.getWidth() + padding_x, Src.getHeight() + padding_y, Bitmap.Config.ARGB_8888);
        Canvas can = new Canvas(outputimage);
        can.drawARGB(color, color, color, color); //This represents White color
        can.drawBitmap(Src, padding_x, padding_y, null);
        return outputimage;
    }

    private int Barcode_BC_UPCA() throws Exception {
        HPRTPrinterHelper.PrintText("BC_UPCA:\n");
        return HPRTPrinterHelper.PrintBarCode(HPRTPrinterHelper.BC_UPCA,
                "075678164125");
    }

    private int Barcode_BC_UPCE() throws Exception {
        HPRTPrinterHelper.PrintText("BC_UPCE:\n");
        return HPRTPrinterHelper.PrintBarCode(HPRTPrinterHelper.BC_UPCE,
                "01227000009");//04252614
    }

    private int Barcode_BC_EAN8() throws Exception {
        HPRTPrinterHelper.PrintText("BC_EAN8:\n");
        return HPRTPrinterHelper.PrintBarCode(HPRTPrinterHelper.BC_EAN8,
                "04210009");
    }

    private int Barcode_BC_EAN13() throws Exception {
        HPRTPrinterHelper.PrintText("BC_EAN13:\n");
        return HPRTPrinterHelper.PrintBarCode(HPRTPrinterHelper.BC_EAN13,
                "6901028075831");
    }

    private int Barcode_BC_CODE93() throws Exception {
        HPRTPrinterHelper.PrintText("BC_CODE93:\n");
        return HPRTPrinterHelper.PrintBarCode(HPRTPrinterHelper.BC_CODE93,
                "TEST93");
    }

    private int Barcode_BC_CODE39() throws Exception {
        HPRTPrinterHelper.PrintText("BC_CODE39:\n");
        return HPRTPrinterHelper.PrintBarCode(HPRTPrinterHelper.BC_CODE39,
                "123456789");
    }

    private int Barcode_BC_CODEBAR() throws Exception {
        HPRTPrinterHelper.PrintText("BC_CODEBAR:\n");
        return HPRTPrinterHelper.PrintBarCode(HPRTPrinterHelper.BC_CODEBAR,
                "A40156B");
    }

    private int Barcode_BC_ITF() throws Exception {
        HPRTPrinterHelper.PrintText("BC_ITF:\n");
        return HPRTPrinterHelper.PrintBarCode(HPRTPrinterHelper.BC_ITF,
                "123456789012");
    }

    private int Barcode_BC_CODE128() throws Exception {
        HPRTPrinterHelper.PrintText("BC_CODE128:\n");
        return HPRTPrinterHelper.PrintBarCode(HPRTPrinterHelper.BC_CODE128,
                "{BS/N:{C\014\042\070\116{A3");    // decimal 1234 = octonary 1442
    }

    private void CapturePrinterFunction() {
        try {
            int[] propType = new int[1];
            byte[] Value = new byte[500];
            int[] DataLen = new int[1];
            String strValue = "";
            boolean isCheck = false;

            int iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_BEEP, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.Buzzer = (Value[0] == 0 ? false : true);

            iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_CUT, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.Cut = (Value[0] == 0 ? false : true);
            btnCut.setVisibility((PrinterProperty.Cut ? View.VISIBLE : View.GONE));

            iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_DRAWER, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.Cashdrawer = (Value[0] == 0 ? false : true);
            btnOpenCashDrawer.setVisibility((PrinterProperty.Cashdrawer ? View.VISIBLE : View.GONE));

            iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_BARCODE, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.Barcode = new String(Value);
            isCheck = PrinterProperty.Barcode.replace("QRCODE", "").replace("PDF417", "").replace(",,", ",").replace(",,", ",").length() > 0;
            btn1DBarcodes.setVisibility((isCheck ? View.VISIBLE : View.GONE));
            isCheck = PrinterProperty.Barcode.contains("QRCODE");
            btnQRCode.setVisibility((isCheck ? View.VISIBLE : View.GONE));
            btnPDF417.setVisibility((PrinterProperty.Barcode.indexOf("PDF417") != -1 ? View.VISIBLE : View.GONE));

            iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_PAGEMODE, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.Pagemode = (Value[0] == 0 ? false : true);
            if (PrinterName.equals("MLP2")) {
                btnPageMode.setVisibility(View.VISIBLE);
            } else {
                btnPageMode.setVisibility((PrinterProperty.Pagemode ? View.VISIBLE : View.GONE));
            }

            iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_GET_REMAINING_POWER, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.GetRemainingPower = (Value[0] == 0 ? false : true);
            btnGetRemainingPower.setVisibility((PrinterProperty.GetRemainingPower ? View.VISIBLE : View.GONE));

            iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_CONNECT_TYPE, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.ConnectType = (Value[1] << 8) + Value[0];
            //btnWIFI.setVisibility(((PrinterProperty.ConnectType&1)==0? View.GONE: View.VISIBLE));
            //btnUSB.setVisibility(((PrinterProperty.ConnectType&16)==0? View.GONE: View.VISIBLE));
            btnBT.setVisibility(((PrinterProperty.ConnectType & 32) == 0 ? View.GONE : View.VISIBLE));

            iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_PRINT_RECEIPT, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.SampleReceipt = (Value[0] == 0 ? false : true);
            btnSampleReceipt.setVisibility((PrinterProperty.SampleReceipt ? View.VISIBLE : View.GONE));
        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> CapturePrinterFunction ")).append(e.getMessage()).toString());
        }
    }

    private void GetPrinterProperty() {
        try {
            int[] propType = new int[1];
            byte[] Value = new byte[500];
            int[] DataLen = new int[1];
            String strValue = "";
            int iRtn = 0;

            iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_STATUS_MODEL, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.StatusMode = Value[0];

            if (PrinterProperty.Cut) {
                iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_CUT_SPACING, propType, Value, DataLen);
                if (iRtn != 0)
                    return;
                PrinterProperty.CutSpacing = Value[0];
            } else {
                iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_TEAR_SPACING, propType, Value, DataLen);
                if (iRtn != 0)
                    return;
                PrinterProperty.TearSpacing = Value[0];
            }

            if (PrinterProperty.Pagemode) {
                iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_PAGEMODE_AREA, propType, Value, DataLen);
                if (iRtn != 0)
                    return;
                PrinterProperty.PagemodeArea = new String(Value).trim();
            }
            Value = new byte[500];
            iRtn = HPRTPrinterHelper.CapturePrinterFunction(HPRTPrinterHelper.HPRT_MODEL_PROPERTY_KEY_WIDTH, propType, Value, DataLen);
            if (iRtn != 0)
                return;
            PrinterProperty.PrintableWidth = (int) (Value[0] & 0xFF | ((Value[1] & 0xFF) << 8));
        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> CapturePrinterFunction ")).append(e.getMessage()).toString());
        }
    }

    private void PrintSampleReceipt() {
        try {
            byte[] data = new byte[]{0x1b, 0x40};
            HPRTPrinterHelper.DirectIO(data, null, 0);
            PAct.LanguageEncode();
            PAct.BeforePrintAction();
            String[] ReceiptLines = getResources().getStringArray(R.array.activity_main_sample_2inch_receipt);
            for (int i = 0; i < ReceiptLines.length; i++)
                HPRTPrinterHelper.PrintText(ReceiptLines[i]);
            PAct.AfterPrintAction();
        } catch (Exception e) {
            Log.e("HPRTSDKSample", (new StringBuilder("Activity_Main --> PrintSampleReceipt ")).append(e.getMessage()).toString());
        }
    }

	/*public static class PrinterProperty
    {
		public static String Barcode="";
		public static boolean Cut=false;
		public static int CutSpacing=0;
		public static int TearSpacing=0;
		public static int ConnectType=0;
		public static boolean Cashdrawer=false;
		public static boolean Buzzer=false;
		public static boolean Pagemode=false;
		public static String PagemodeArea="";
		public static boolean GetRemainingPower=false;
		public static boolean SampleReceipt=true;
		public static int StatusMode=0;
	}*/
}
