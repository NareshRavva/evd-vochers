package com.vstk.printer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.Toast;

import HPRTAndroidSDK.HPRTPrinterHelper;
import com.vstk.R;

public class Activity_PDF417  extends Activity
{	
	private Context thisCon=null;
	private ArrayAdapter arrQRCodeSize;
	private ArrayAdapter arrQRCodeModel;
	private ArrayAdapter arrQRCodeLevel;
	private EditText txtpdf417Data=null;
	private EditText txtpdf417dataColumns=null;
	private EditText txtpdf417dataRows=null;
	private RadioButton rb_pdf417_hierarchicalMode=null;
	private RadioButton rb_pdf417_RatioMode=null;
	private RadioButton rb_pdf417_standardMode=null;
	private RadioButton rb_pdf417_compressionMode=null;
	private RadioGroup rg_pdf417_errorMode=null;
	private RadioGroup rg_pdf417_options=null;
	private int errorMode=0;
	private int pdf417moduleWidth=3;
	private int pdf417rowHeight=3;
	private int errorLevel=0;
	private int options=0;
	private Spinner spnpdf417moduleWidth;
	private Spinner spnpdf417rowHeight;
	private Spinner spn_pdf417_errorLevel;
	
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);	   
		this.requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.p_activity_pdf417);
		thisCon=this.getApplicationContext();
		
		txtpdf417Data=(EditText)this.findViewById(R.id.txtpdf417Data);
		txtpdf417dataColumns=(EditText)this.findViewById(R.id.txtpdf417dataColumns);
		txtpdf417dataRows=(EditText)this.findViewById(R.id.txtpdf417dataRows);
		spnpdf417moduleWidth = (Spinner) findViewById(R.id.spnpdf417moduleWidth);
		spnpdf417rowHeight = (Spinner) findViewById(R.id.spnpdf417rowHeight);
		rg_pdf417_errorMode=(RadioGroup)this.findViewById(R.id.rg_pdf417_errorMode);
		rb_pdf417_hierarchicalMode=(RadioButton)this.findViewById(R.id.rb_pdf417_hierarchicalMode);
		rb_pdf417_RatioMode=(RadioButton)this.findViewById(R.id.rb_pdf417_RatioMode);
		spn_pdf417_errorLevel = (Spinner) findViewById(R.id.spn_pdf417_errorLevel);
		rg_pdf417_options=(RadioGroup)this.findViewById(R.id.rg_pdf417_options);
		rb_pdf417_standardMode=(RadioButton)this.findViewById(R.id.rb_pdf417_standardMode);
		rb_pdf417_compressionMode=(RadioButton)this.findViewById(R.id.rb_pdf417_compressionMode);
		rg_pdf417_errorMode.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId==rb_pdf417_hierarchicalMode.getId()) {
					errorMode=0x48;
				}else if (checkedId==rb_pdf417_RatioMode.getId()) {
					errorMode=0x49;
				}
			}
		});
		rg_pdf417_options.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId==rb_pdf417_standardMode.getId()) {
					options=0;
				}else if (checkedId==rb_pdf417_compressionMode.getId()) {
					options=1;
				}
			}
		});
		
		String[] sList;
		sList="2,3,4,5,6,7,8".split(",");		
		arrQRCodeSize = new ArrayAdapter<String>(Activity_PDF417.this,android.R.layout.simple_spinner_item, sList);
		arrQRCodeSize.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnpdf417moduleWidth.setAdapter(arrQRCodeSize);
		spnpdf417moduleWidth.setOnItemSelectedListener(new OnItemSelectedQRCodeSize());
		
		/*spnQRCodeModel = (Spinner) findViewById(R.id.spnQRCodeModel);	
		sList="1,2".split(",");	*/	
		arrQRCodeModel = new ArrayAdapter<String>(Activity_PDF417.this,android.R.layout.simple_spinner_item, sList);
		arrQRCodeModel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spnpdf417rowHeight.setAdapter(arrQRCodeModel);
		spnpdf417rowHeight.setOnItemSelectedListener(new OnItemSelectedQRCodeModel());
		
//		spnQRCodeLevel = (Spinner) findViewById(R.id.spnQRCodeLevel);	
		sList="0,1,2,3,4,5,6,7,8".split(",");		
		arrQRCodeLevel = new ArrayAdapter<String>(Activity_PDF417.this,android.R.layout.simple_spinner_item, sList);
		arrQRCodeLevel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);		
		spn_pdf417_errorLevel.setAdapter(arrQRCodeLevel);
		spn_pdf417_errorLevel.setOnItemSelectedListener(new OnItemSelectedQRCodeLevel());
	}

	private class OnItemSelectedQRCodeSize implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{			
			pdf417moduleWidth=arg2+2;
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}
	
	private class OnItemSelectedQRCodeModel implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{			
			pdf417rowHeight=arg2+2;
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}
	
	private class OnItemSelectedQRCodeLevel implements OnItemSelectedListener
	{				
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3)
		{			
			errorLevel=arg2+0x48;
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0)
		{
			// TODO Auto-generated method stub			
		}
	}
	
	public void onClickPrint(View view)
	{
    	if (!checkClick.isClickEvent()) return;
    	
    	try
    	{
	    	if(txtpdf417Data.getText().toString().trim().length()==0)
	    	{
	    		Toast.makeText(thisCon, getString(R.string.activity_pdf417_no_data), Toast.LENGTH_SHORT).show();
	    		return;
	    	}
	    	if(txtpdf417dataColumns.getText().toString().trim().length()==0)
	    	{
	    		Toast.makeText(thisCon, getString(R.string.activity_pdf417_no_dataColumns), Toast.LENGTH_SHORT).show();
	    		return;
	    	}
	    	if(txtpdf417dataRows.getText().toString().trim().length()==0)
	    	{
	    		Toast.makeText(thisCon, getString(R.string.activity_pdf417_no_dataRows), Toast.LENGTH_SHORT).show();
	    		return;
	    	}
	    	int columns = Integer.valueOf(txtpdf417dataColumns.getText().toString());
	    	int Rows = Integer.valueOf(txtpdf417dataRows.getText().toString());
	    	PublicAction PAct=new PublicAction(thisCon);
	    	PAct.BeforePrintAction();
	    	//HPRTPrinterHelper.SetJustification(justification+0x30);
	    	//HPRTPrinterHelper.PRTPrintBarcode2(txtpdf417Data.getText().toString(), QRCodeSize+1, QRCodeLevel+0x30,justification);
	    	//HPRTPrinterHelper.PrintQRCode_MPT(txtQRCodeData.getText().toString(),(QRCodeLevel+0x30),(QRCodeSize+1),justification);
			HPRTPrinterHelper.PrintPDF417(txtpdf417Data.getText().toString(), (byte)columns, (byte)Rows, (byte)pdf417moduleWidth, (byte)pdf417rowHeight, (byte)errorMode, (byte)errorLevel, (byte)options);
	    	PAct.AfterPrintAction();
    	}
		catch (Exception e)
		{			
			Log.d("HPRTSDKSample", (new StringBuilder("Activity_QRCode --> onClickPrint ")).append(e.getMessage()).toString());
		}
    }
}
