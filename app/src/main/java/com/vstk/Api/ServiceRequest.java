package com.vstk.Api;

import android.content.Context;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import com.vstk.logger.Logger;
import com.vstk.utils.AppUtils;
import com.vstk.utils.SharedPrefsUtils;

import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by Naresh R on 03-09-2016.
 */
public class ServiceRequest {

    private static String TAG = "ServiceRequests";

    private static TypedInput in;

    public static TypedInput returnTypedArr(Context context, ServiceMethods method, Object[] parameters) {
        //  if (parameters == null)
        // return null;

        JSONObject jsonObj = new JSONObject();
        JSONObject criteria;
        JSONObject sort;

        try {
            switch (method) {

                case USER_LOGIN:
                    jsonObj.put("UserName", parameters[0]);
                    jsonObj.put("Password", parameters[1]);
                    jsonObj.put("DeviceType", 1);
                    jsonObj.put("DeviceOS", "Android");
                    jsonObj.put("DeviceModel", android.os.Build.MODEL);
                    jsonObj.put("DeviceIME", parameters[2]);
                    jsonObj.put("DeviceOSVersion", android.os.Build.VERSION.RELEASE);
                    break;

                case GET_PRODUCTS_BY_VENDOR_ID:
                case GET_VENDORS_BY_ID:
                    jsonObj.put("UserName", SharedPrefsUtils.getUserName(context));
                    jsonObj.put("UserShift", 1);
                    jsonObj.put("Password", SharedPrefsUtils.getUserPaswrd(context));
                    jsonObj.put("TerminalId", SharedPrefsUtils.getTerminalID(context));
                    jsonObj.put("UniqueReferenceNum", AppUtils.convertMilleTodate("" + System.currentTimeMillis()));
                    jsonObj.put("Authcode", SharedPrefsUtils.getAuthID(context));
                    jsonObj.put("Description", "xcvxc");
                    jsonObj.put("Comment", "Comment");
                    break;

                case DO_PAYMENY:

                    JSONObject headrObjt = new JSONObject();
                    headrObjt.put("UserName", SharedPrefsUtils.getUserName(context));
                    headrObjt.put("UserShift", 1);
                    headrObjt.put("Password", SharedPrefsUtils.getUserPaswrd(context));
                    headrObjt.put("TerminalId", SharedPrefsUtils.getTerminalID(context));
                    headrObjt.put("UniqueReferenceNum", AppUtils.convertMilleTodate("" + System.currentTimeMillis()));
                    headrObjt.put("Authcode", SharedPrefsUtils.getAuthID(context));
                    headrObjt.put("Comment", "aadsfadf");
                    jsonObj.put("headerinputs", headrObjt);

                    JSONObject datarequestObjt = new JSONObject();
                    datarequestObjt.put("supplierId", parameters[0]);
                    datarequestObjt.put("productId", parameters[1]);
                    datarequestObjt.put("denomcode", parameters[2]);
                    datarequestObjt.put("quantity", "1");
                    jsonObj.put("datarequest", datarequestObjt);

                    break;


                default:
                    break;
            }
        } catch (Exception e) {
            Logger.info(TAG, "Error while creating " + method.toString() + " request" + e.toString());
        }

        Logger.info(TAG, jsonObj.toString());

        try {
            in = new TypedByteArray("application/json", jsonObj.toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return in;
    }


}
