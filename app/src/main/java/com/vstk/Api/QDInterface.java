package com.vstk.Api;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.mime.TypedInput;

/**
 * Created by Naresh R on 02-09-2016.
 */
public interface QDInterface {

    @POST("/Authenticate")
    void userLogin(@Header("Content-Type") String cType, @Body TypedInput tInput, Callback<Response> responseCallback);


    @POST("/GetVendorList/Cid={Cid}")
    void getVendorsList(@Path("Cid") String Cid, @Header("Content-Type") String cType, @Body TypedInput tInput, Callback<Response> responseCallback);


    @POST("/GetVendors/Cid={Cid}/vid={id}")
    void getProducts(@Path("Cid") String Cid, @Path("id") String id, @Header("Content-Type") String cType, @Body TypedInput tInput, Callback<Response> responseCallback);


    @POST("/DoPayment")
    void doPayment(@Header("Content-Type") String cType, @Body TypedInput tInput, Callback<Response> responseCallback);


}
